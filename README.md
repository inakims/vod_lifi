#Configuarción para inicio del entorno
Abrir 3 ventanas del terminal
1.  cd vod/server  
    sudo docker-compose up

    _Con esto queda arrancado el servidor web de la plataforma vod. En este servidor también se encuentra la base de datos de assets así como la base de datos de CDN's (todavía no implementado)_

2.  cd vod/cdn/test3  
    sudo docker-compose up  

    _Con esto quedan arrancados los dos servidores de contenido que se van a utilizar en la demo, arrancados en el puerto 8000 y 8001 drespectivamente._

3.  cd conn  
    npm start  

    _Con esto queda arrancado el conector. Su funcionamiento en esta versión es recibir por UDP y mandar por WS al cliente web cuando este inicie la reproducción_


En esta versión no se hace ninguna discriminación en función del mensaje que el actuador envía. En cuanto el cliente recibe un mensaje por WS realiza un cambio de CDN de manera autónoma.

# Importante
Para poder ejecutar _docker-compose_ es necesario tener todo lo necesario (datos) en una unidad con sistema de ficheros _Ext4_ si no Mongo da un error.

Para poder ejecutar el escenario hay que tener en cuenta:

-   Tener bien definido en el fichero server.js (server/backend) el campo Control-Allow-Origin
-   Cambiar de app.config.js la dirección del backend (debe estar en localhost)
-   Dirección a la cual se conecta el cliente WS (debe ser localhost)
-   Dirección en la cual se ubica el conector. Poner IP en el fichero de configuración del Agent 2.0
