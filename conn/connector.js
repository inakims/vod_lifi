var dgram = require('dgram');
var server = dgram.createSocket('udp4');
var webSocket = require('ws');
var eventEmitter = require('events');

class myEmitter extends eventEmitter{}

//var udp

var actionReceived = false;

/////////
// Se añade al mensaje la IP del origen de quien emite el mensaje UDP

/////////

// Gestión de la conexión UDP -- Q4S

server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});

/*server.on('message', (msg, rinfo) => {
    console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
    console.log (typeof(msg.toString()) + ' : ' + msg);
    actionReceived = true;
});*/

server.on('listening', () => {
    const address = server.address();
    console.log(`Q4S listening on ${address.address}:${address.port}`);
});

server.bind(27018);

// Gestión de la conexión WebSocket -- Cliente web

var wss = new webSocket.Server({port: 7000});

function noop() {}

function heartbeat() {
  this.isAlive = true;
  console.log('PONG');
}

wss.on('connection', function connection(ws) {
  ws.isAlive = true;
  ws.on('pong', heartbeat);
});

const interval = setInterval(function ping() {
  console.log('PING');
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();

    ws.isAlive = false;
    ws.ping(noop);
  });
}, 3000);

wss.on('connection', function (ws){
    console.log('connected');

    server.on('message', (msg, rinfo) => {
        console.log(`server got: ${msg} from ${rinfo.address}`);
        var mes = {'text': msg.toString(), 'src': rinfo.address};
        ws.send(JSON.stringify(mes));
    });
})