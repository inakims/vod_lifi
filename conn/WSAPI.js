var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var webSocket = require('ws');

var app = express();

var wss = new webSocket.Server({port: 7000});

app.use(bodyParser.json());

wss.on('connection', function (ws){
    console.log('connected');
    console.log('TEST');

    app.post('/', function (req, res){
    	console.log(req.body);
    	res.send('POST  "/" received\n');
    	ws.send('TEST WS');
    })

    ws.on('close', function(){
        console.log('closing connection');
    })
})

app.listen(3500, function (){
	console.log('WS API running');
});

console.log('TEST');

// Guardar el ws abierto una vez conectado y utilizarlo para mandar info
// Cuando se de el evento de mensaje UDP comprobar si existe conexión con
// el ws y si existe se manda