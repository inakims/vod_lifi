var dgram = require('dgram');
var server = dgram.createSocket('udp4');
var webSocket = require('ws');
var eventEmitter = require('events');

process.argv.forEach((val, index) => {
	console.log(`$(index): $(val)`);
});

class myEmitter extends eventEmitter{}

var udp

var actionReceived = false;

// Gestión de la conexión UDP -- Q4S

server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});

/*server.on('message', (msg, rinfo) => {
    console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
    console.log (typeof(msg.toString()) + ' : ' + msg);
    actionReceived = true;
});*/

server.on('listening', () => {
    const address = server.address();
    console.log(`Q4S listening on ${address.address}:${address.port}`);
});

server.bind(27018);

// Gestión de la conexión WebSocket -- Cliente web

var wss = new webSocket.Server({port: 7000});

wss.on('connection', function (ws){
    console.log('connected');

    server.on('message', (msg, rinfo) => {
        console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
        ws.send(msg.toString());
    });

    ws.on('close', function(){
        console.log('closing connection');
    })
})

// Guardar el ws abierto una vez conectado y utilizarlo para mandar info
// Cuando se de el evento de mensaje UDP comprobar si existe conexión con
// el ws y si existe se manda