#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main()
{

    FILE *diag_ojos_send, *diag_ojos_rec;
    int fd1,fd2;

    /* SEND */
    diag_ojos_send = fopen ("diag_ojos_send.dat","rb");
    fseek(diag_ojos_send,0,SEEK_END);
    int tam_send=ftell(diag_ojos_send);
    printf ("TAMAÑO SEND = %d\n",tam_send);
    fclose(diag_ojos_send);

    /* RECEIVE */
    diag_ojos_rec = fopen ("diag_ojos_rec.dat","rb");
    fseek(diag_ojos_rec,0,SEEK_END);
    int tam_rec=ftell(diag_ojos_rec);
    printf ("TAMAÑO REC = %d\n",tam_rec);
    fclose(diag_ojos_rec);
    
    int tam=0;
    
    if (tam_rec>tam_send){
	
	tam=tam_send;
    }
	
    else{

	tam=tam_rec;	
    }

    char *buffer_send=malloc(tam), *buffer_rec=malloc(tam);;
    fd1=open("diag_ojos_send.dat",O_RDWR);
    read(fd1,buffer_send,tam);
    fd2=open("diag_ojos_rec.dat",O_RDWR);
    read(fd2,buffer_rec,tam);

    int x=0;
    int bien=0,mal=0;
    char *test1=malloc(1),*test2=malloc(1);
    for(x=0;x<tam;x++){

        memcpy((char *)test1,(char *)buffer_send+x,1);
        memcpy((char *)test2,(char *)buffer_rec+x,1);
        if(((*test1=='0') && (*test2=='0')) || ((*test1=='1') && (*test2=='1'))){

            bien++;
        }
        else{
            mal++;
        }

    }

    printf("BIEN = %d\n",bien);
    printf("MAL = %d\n",mal);
    int total=bien+mal;
    printf("TOTAL = %d\n",total);
    float porcentaje;
    porcentaje=bien;
    porcentaje=(float)(porcentaje/total); 
    porcentaje=porcentaje*100;
    printf("\nPorcentaje de bits correctos = %f\n\n",porcentaje);

    return 0;
}
