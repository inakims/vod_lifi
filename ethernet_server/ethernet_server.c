/***** HEADERS *****/
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#include <pthread.h>

#define USB_BUF_SIZ 2500
//#define USB_DEFAULT_IF "enp0s25"
#define USB_DEFAULT_IF "wlp2s0"

void *execScriptThread(void *vargp)
{
	char *script;
	script=(char*)vargp;
    //sleep(6);
    printf("Executing Script => '%s'\n",script);
	system(script);
    return NULL;
}

int main()
{
	int sockfd;
	int sockopt;
	ssize_t numbytes;
	struct ifreq ifopts;	/* set promiscuous mode */
	struct ifreq if_ip;	/* get ip addr */
	uint8_t buf[USB_BUF_SIZ];
	char ifName[IFNAMSIZ];

	/* Interface name */
	strcpy(ifName, USB_DEFAULT_IF);

	/* Header structures */
	memset(&if_ip, 0, sizeof(struct ifreq));

	/* Open PF_PACKET socket, listening for EtherType ETHER_TYPE */
	if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1)
	{
		printf("listener: socket\n");
	}

	/* Set interface to promiscuous mode */
	strncpy(ifopts.ifr_name, ifName, IFNAMSIZ-1);
	ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
	ifopts.ifr_flags |= IFF_PROMISC;
	ioctl(sockfd, SIOCSIFFLAGS, &ifopts);

	/* Allow the socket to be reused - incase connection is closed prematurely */
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1)
	{
		printf("setsockopt\n");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	/* Bind to device */
	if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ-1) == -1)
	{
		printf("SO_BINDTODEVICE\n");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	int x=0, end=0;
	pthread_t thread_id = 0;
	while(1){
		
		numbytes = recvfrom(sockfd, buf, USB_BUF_SIZ, 0, NULL, NULL);

		/*if((buf[30]==192)&&(buf[31]==168)&&(buf[32]==0)&&(buf[33]==100)) //Destination 192.168.0.100 zybo
		{
			if((buf[26]==192)&&(buf[27]==168)&&(buf[28]==0)&&(buf[29]==101)) //Source 192.168.0.101 zybo
			{*/

		if((buf[30]==147)&&(buf[31]==96)&&(buf[32]==157)&&(buf[33]==97)) //Destination 147.96.157.97 eduroam wifi UCM
		{
			if((buf[26]==147)&&(buf[27]==96)&&(buf[28]==157)&&(buf[29]==97)) //Source 147.96.157.97 eduroam wifi UCM
			{
				
				int y=0;
				for(y=0;y<numbytes;y++){	
					//printf("Buffer [%d] = %c\n",y,buf[y]);

					if(buf[y]=='#' && y>42){
						end = y;
						//printf("He llegado al final!! = %d\n",y);
					} 
				}

				int size = end-42;
				//printf("Size: %d\n",size );

				uint8_t video[size], lastVideo[size];

				memcpy(&video[0], &buf[42], size);
				//printf("Size: %zu\n",sizeof(video));

				//printf("Requested Video = %s\n",video);

				/*int j = 0;
				for(j=0;j<size;j++){
					printf("Video [%d] = %c\n",j,video[j]);
					printf("Last Video [%d] = %c\n",j,lastVideo[j]);
				}*/

				if(memcmp(lastVideo,video,size-5) != 0){
					memcpy(&lastVideo[0], &video[0], size);	

					printf("Thread id:%zu\n",thread_id);

					if(thread_id != 0){
						/*int result = 0;
						result = pthread_cancel(thread_id);
						printf("Result?? = %d\n",result);*/

						system("killall vlc");
					}
					
					if(strcmp(video,"Baby_Driver") == 0){
						printf("Reproducing Video = %s\n",video);
						//pthread_t thread_id;
					    //printf("Before Thread\n");
					    pthread_create(&thread_id, NULL, execScriptThread, "sh video1.sh");
					    //pthread_join(thread_id, NULL);
					    //printf("After Thread\n");
						//system("sh video1.sh");
						//system("vlc-wrapper -vvv video1.mp4 --sout '#transcode{vcodec=h264,vb=800,acodec=mpga,ab=128,channels=2,samplerate=44100}:udp{dst=127.0.0.1:1234}'");
					}
					else if(strcmp(video,"War_for_the_Planet_of_the_Apes") == 0){
						printf("Reproducing Video = %s\n",video);
						//pthread_t thread_id;
					    //printf("Before Thread\n");
					    pthread_create(&thread_id, NULL, execScriptThread, "sh video2.sh");
					    //pthread_join(thread_id, NULL);
					    //printf("After Thread\n");
						//system("sh video2.sh");
						//system("vlc-wrapper -vvv video2.mp4 --sout '#transcode{vcodec=h264,vb=800,acodec=mpga,ab=128,channels=2,samplerate=44100}:udp{dst=127.0.0.1:1234}'");
					}
					else{
						printf("Reproducing Video = %s\n",video);
						//pthread_t thread_id;
					    //printf("Before Thread\n");
					    pthread_create(&thread_id, NULL, execScriptThread, "sh video1.sh");
					    //pthread_join(thread_id, NULL);
					    //printf("After Thread\n");
						//system("sh video1.sh");
						//system("vlc-wrapper -vvv video1.mp4 --sout '#transcode{vcodec=h264,vb=800,acodec=mpga,ab=128,channels=2,samplerate=44100}:udp{dst=127.0.0.1:1234}'");
					}

					//printf("Thread id:%zu\n",thread_id);
				}

				memset(video, 0, sizeof(video));
			}
		}
	}
	
	close(sockfd);
	return 0;
}


