#!/bin/bash

vlc-wrapper -vvv video2.mp4 --sout '#transcode{vcodec=h264,vb=800,acodec=mpga,ab=128,channels=2,samplerate=44100}:udp{dst=127.0.0.1:1234}'
