# Prueba de codificación de un video mp4 a mpeg-dash
Para hacer la codificación se codifica por un lado el vídeo en las calidades que se requieran y por otro lado el audio (habitualmente a una calidad para todos los anchos de banda)

A continuación con MP4Box se genera el manifest y se divide el archivo según los parámetros que se le pasan.

El player es quien solicita automáticamente los segmentos de vídeo.