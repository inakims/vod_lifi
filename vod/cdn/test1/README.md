# Alojamiento de contenidos en un servidor externo
Todo el contenido que sirve la web se ha divido en dos.
- Servidor web (html, css, js, imágenes para renderizar la web)
- Servidor de contenido (trailers)

De este modo se puede reducir la carga de trabajo del servidor único que se tenía anteriormente. 

Para esto se ha implementado:
- Cuando el usuario da PLAY a cualquier contenido se hace una petición al backend para que le devuelva una lista con los 3 CDN más cercanos al usuario según su IP (ordenados por cercanía? por Q4S desde el servidor? aleatorio? --> aleatorio es válido ya que en un futuro el usuario comprobará cual de los 3 tiene mejor calidad y los "ordenará").
- El usuario extrae de ese json con los CDN y utiliza la ip para enviar la petición del contenido de video.