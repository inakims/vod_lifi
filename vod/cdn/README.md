# Idea de funcionamiento del sistema de CDN
El contenido de video es servido por un 3º servidor para separar la distribución de la web de la distribución de contenido.

C --(web)--> S
C <--(web)-- S

C --(play)--> S | Servidor identifica localización de cliente y genera
                | json con las ip de los servidores CDN más cercanos
                | (óptimos).
C <--(play)-- S | Cliente recibe json y elige el primero para renderizar 
                | el HTML con la url correcta del CDN.

C --(content)--> CDN1
C <--(content)-- CDN1