'use strict';

// var http = require('http');
var bodyParser = require('body-parser'),
    requestIp = require('request-ip'),
    express = require('express'),
    webSocket = require('ws'),
    app = express(),
    wsPort = 7000,
    srvPort = 3500,
    wss = new webSocket.Server({port: wsPort});
    //wss = new webSocket.Server('ws://192.168.1.126:7000');

app.use(bodyParser.json());
app.use(requestIp.mw());


wss.on('connection', function(ws) {
    console.log('connected test');

    app.post('/updatelevel', function(req, res, ws) {
        console.log('----------------------');
        console.log(`Received POST`);
        console.log(req.clientIp.split(':')[3]);
        console.log(req.body);
        console.log('----------------------');
        ws.send(JSON.stringify({'msg':'updatelevel', 'ip': req.clientIp.split(':')[3],
            'level': req.body.level}));
    });

    app.post('/switchbest', function(req, res) {
        console.log('----------------------');
        console.log(`Received POST SW`);
        console.log(req.clientIp.split(':')[3]);
        console.log(req.body);
        console.log('----------------------');
        console.log(JSON.stringify({'msg': 'switchbest'}));
        res.send('POST  "/" received\n');
        ws.send(JSON.stringify({'msg':'switchbest'}));
    });

    ws.on('close', function() {
        console.log('closing connection');
    });
});
  



app.listen(srvPort, function() {
    console.log(`WS API running on port:${srvPort}`);
});
