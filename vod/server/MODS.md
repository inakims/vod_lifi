PARA PODER EJECUTAR LA PLATAFORMA DE VOD EN CUALQUIER ORDENADOR ES NECESARIO CAMBIAR:
-   vod/server/webserver/backend/server.js => Acces-Control-Allow-Origin <http://host_address>
-   vod/server/webserver/frontend/app/app.config.js => backend: <http://host_address>



Modificaciones realizadas sobre el código del repositorio (/Descargas)
-   Cambiar mockup a false en config
-   Cambiar IP L 56 -> server.js --> IP Access-Control-Allow-Origin

DB
-   En db1 está la base de datos completa con TODOS los assets (series, películas categorias...)
-   Se eliminan todos los documentos que no sean películas o categorías
	Se mantiene type:category

**El acceso a la api se hace de 2 maneras:
	- puerto 3000
	- puerto 5080 con /api
Hay que ajustar bien el direccionamiento en app-config.js => backend**

-   Cambiar la carga del video bad_moms.mp4 por el archivo de TRAILERS correspondiente por id.mp4
