var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentsSchema   = new Schema({
    id: String,
    title: String,
    description: String,
    users:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Users'
    },
    asset:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Asset'
    }
    
});

module.exports = mongoose.model('Comments', CommentsSchema);