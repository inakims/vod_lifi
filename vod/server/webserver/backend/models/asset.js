var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AssetSchema   = new Schema({
    id: String,
    title: String,
    description: String,
    IMBDrating: Number,
    tags: String,
    length: Number,
    numberSeason: Number,
    year: Number,
    prizes: String,
    comingUp: String,
    rate: Number,
    seasonCount: Number,
    episodeCount: Number,
    img: String,
    items: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Asset'
        }
    ],
    video: String,
    type: String,
    acronym: String,
    filmactor: String,
    actor: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Actor'
        }
    ]

});

module.exports = mongoose.model('Asset', AssetSchema);