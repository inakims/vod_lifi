var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActorSchema   = new Schema({
    name: String,
    surname: String,
    fullname: String,
    pic: String,
    description: String,
    rank: Number,
    acting:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Asset'
        }]
	});

module.exports = mongoose.model('Actor', ActorSchema);