var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookmarkingSchema   = new Schema({
    
    watchedDate: Date,
    watchedTime: String,
    seen: Boolean,
    mood: String,
    assetId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset'
    }

});

module.exports = mongoose.model('Bookmarkings', BookmarkingSchema);