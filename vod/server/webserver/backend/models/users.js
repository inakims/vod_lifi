var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UsersSchema   = new Schema({
    username: String,
    name: String,
    surname: String,
    email: String,
    password: String,
    watching: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Bookmarkings'
        }
    ],
    fav: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Asset'
        }
    ],
    mylist: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Asset'
        }
    ]
});

module.exports = mongoose.model('Users', UsersSchema);