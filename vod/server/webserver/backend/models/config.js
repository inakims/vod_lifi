var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConfigSchema   = new Schema({
    refresh_in: Number,
    root: [
        {
            enable: Boolean,
            broadcast: Boolean,
            path: String,
            service_id: String
        }
    ]
});

module.exports = mongoose.model('Config', ConfigSchema);