var mongoose = require('mongoose');

var Schema = mongoose.Schema;



var CategorySchema   = new Schema({
    title: String,
    type: String,
    assets: [

        {

            type: mongoose.Schema.Types.ObjectId,

            ref: 'Asset'

        }

    ]

});
module.exports = mongoose.model('Category', CategorySchema);
