var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ServersSchema = new Schema({
    name: String,
    ip_address: String,
    port: Number
});

module.exports = mongoose.model('Servers', ServersSchema);
