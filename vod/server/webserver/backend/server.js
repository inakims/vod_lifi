'use strict';

var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),

    config = require('./config'),

    port = process.env.PORT || 3000,
    NO_TOKEN_PROVIDED = 'No token provided.',
    FAILED_TOKEN = 'Token validation has failed',

    usersRoutes = require('./routes/users'),
    actorRoutes = require('./routes/actor'),
    bookmarkingRoutes = require('./routes/bookmarking'),
    assetRoutes = require('./routes/asset'),
    commentsRoutes = require('./routes/comments'),
    categoryRoutes = require('./routes/category'),
    authRoutes = require('./routes/auth'),
    configService = require('./routes/service');

mongoose.connect('mongodb:27017');
// mongoose.connect('localhost:27017');
app.set('superSecret', config.secret);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(morgan('dev'));

app.use(function(req, res, next) {
    // res.header("Access-Control-Allow-Origin", "*");
    //* *********************************************************************************
    //IÑAKI
    res.header('Access-Control-Allow-Origin', 'http://127.0.0.1');
    //res.header('Access-Control-Allow-Origin', 'http://192.168.1.107');
    //IÑAKI
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    // res.header("Access-Control-Allow-Credentials", true);
    next();
});

// Non protected services

app.use('/vod/users', usersRoutes);
app.use('/vod/bookmarking', bookmarkingRoutes);
app.use('/vod/comments', commentsRoutes);
app.use('/vod/asset', assetRoutes);
app.use('/vod/category', categoryRoutes);
app.use('/vod/auth', authRoutes);
app.use('/vod/service', configService);

/* var cors = require('cors')
var app = express()
app.use(cors({ origin: true, credentials: true }))
app.options(cors({ origin: true, credentials: true }))*/

app.use(function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        console.log('aqui');
        console.log(app.get('superSecret'));
        console.log(token);
        jwt.verify(token, 'secretKey', function(err, decoded) {
            if (err) {
                console.log('aqui3: ' + err);

                return res.json({success: false, message: FAILED_TOKEN});
            } else {
                req.decoded = decoded;
                console.log('aqui2');

                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: NO_TOKEN_PROVIDED
        });
    }
});

// Protected services
// app.use('/test', testRoutes);
app.use('/vod/actor', actorRoutes);


// error managemer
app.use(function(err, req, res, next) {
    res.status(500).send('Internal Server Error: ' + err);
});

app.listen(port);
console.log('API running on localhost:' + port);
