'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Config = require('../models/config');

    router.get('/', function(req, res, next) {
        Config.find({}, function(err, config) {
            if (err) {
                return next(err);
            }

            res.json(config);
        });
    });

    router.put('/', function(req, res, next) {
        Config.find({}, function(err, config) {
            if (err) {
                return next(err);
            }

            if(config.length) {
                var cnf = config[0];

                for(var key in req.body) {
                    if(cnf[key]) {
                        cnf[key] = req.body[key];
                    }
                }

                cnf.save(function(err) {
                    if (err) {
                        return next(err);
                    }

                    res.json(cnf);
                });
            }
        });
    });

    /**
     * [createFirstConf description]
     */
    function createFirstConf() {
        var services = [
            {
                path: 'survey',
                service_id: 'SURVEY'
            },
            {
                path: 'logs',
                service_id: 'LOG'
            }];

        Config.find({}, function(err, config) {
            if(!config.length) {
                var conf = new Config;
                var i;

                conf.refresh_in = 15;

                for(i=0; i<services.length; i+=1) {
                    conf.enable = true;
                    conf.root.push({
                        enable: true,
                        broadcast: false,
                        path: 'reporting/' + services[i].path,
                        service_id: services[i].service_id
                    });
                }

                conf.save();
            }
        });
    }

    createFirstConf();

    return router;
})();
