'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Users = require('../models/users');

    router.get('/', function(req, res, next) {
        var params = {};

        Users.find(params)
            .populate('mylist')
            .populate('fav')
            .populate('watching')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }

                res.json(aux);
            });
    });


    router.post('/', function(req, res, next) {
        var user = new Users();

        for(var key in req.body) {
            if(req.body.hasOwnProperty(key)) {
                user[key] = req.body[key];
            }
        }

        user.save(function(err) {
            if (err) {
                return next(err);
            }

            res.json(user);
        });
    });

    router.get('/:_id', function(req, res, next) {
        Users.findById(req.params._id)
            .populate('mylist')
            .populate('fav')
            .populate('watching')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }

                res.json(aux);
            });
    });


    router.put('/:_id', function(req, res, next) {
        Users.findById(req.params._id, function(err, user) {
            if (err) {
                return next(err);
            }

            for(var key in req.body) {
                if(user[key]) {
                    user[key] = req.body[key];
                }
            }

            user.save(function(err) {
                if (err) {
                    return next(err);
                }

                res.json(user);
            });
        });
    });

    router.delete('/:_id', function(req, res, next) {
        Users.remove({
            _id: req.params._id
        }, function(err, user) {
            if (err) {
                return next(err);
            }

            res.json({message: 'Successfully deleted'});
        });
    });

    return router;
})();
