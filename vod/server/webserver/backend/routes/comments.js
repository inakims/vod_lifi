'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Comments = require('../models/comments'),
        shortId = require('short-mongo-id');

    router.get('/', function(req, res, next) {
        var params = {};
        if(req.query.mac) {
            params.mac = req.query.mac.toLowerCase();
        }
        Comments.find(params)
            .populate('asset users')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }
                res.json(aux);
            });
    });

    router.post('/', function(req, res, next) {
        var comments = new Comments();
        comments.id = shortId(comments._id);
        comments.title = req.body.title || '-';
        comments.description = req.body.description || '-';
        comments.users = req.body.users;
        comments.asset = req.body.asset;
        comments.save(function(err) {
            if (err) {
                return next(err);
            }
            res.json(comments);
        });
    });


    router.get('/:input', function(req, res, next) {
        if (req.params.input.length == 6) {
            Comments.findOne({id: req.params.input})
                .populate('asset users')
                .exec(function(err, comments) {
                    if (err) {
                        return next(err);
                    }
                    res.json(comments);
                });
        }
        if (req.params.input.length == 24) {
            Comments.findById(req.params.input)
                .populate('asset users')
                .exec(function(err, comments) {
                    if (err) {
                        return next(err);
                    }
                    res.json(comments);
                });
        }
    });

    router.put('/:_id', function(req, res, next) {
        Comments.findById(req.params._id, function(err, comments) {
            if (err) {
                return next(err);
            }
            for(var key in req.body) {
                if(comments[key]) {
                    comments[key] = req.body[key];
                }
            }
            comments.save(function(err) {
                if (err) {
                    return next(err);
                }
                res.json(comments);
            });
        });
    });

    router.delete('/:_id', function(req, res, next) {
        Comments.remove({
            _id: req.params._id
        }, function(err, comments) {
            if (err) {
                return next(err);
            }

            res.json({message: 'Successfully deleted'});
        });
    });

    return router;
})();
