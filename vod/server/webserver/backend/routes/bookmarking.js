'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Bookmarkings = require('../models/bookmarking');

    router.get('/', function(req, res, next) {
        var params = {};

        Bookmarkings.find(params)
            .populate('assetId')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }

                res.json(aux);
            });
    });

    router.post('/', function(req, res, next) {
        var bookmarkings = new Bookmarkings();

        for(var key in req.body) {
            if(req.body.hasOwnProperty(key)) {
                bookmarkings[key] = req.body[key];
            }
        }

        bookmarkings.save(function(err) {
            if (err) {
                return next(err);
            }

            res.json(bookmarkings);
        });
    });

    router.get('/:_id', function(req, res, next) {
        Bookmarkings.findById(req.params._id)
            .populate('assetId')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }

                res.json(aux);
            });
    });

    router.put('/:_id', function(req, res, next) {
        Bookmarkings.findById(req.params._id, function(err, bookmarkings) {
            if (err) {
                return next(err);
            }

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)) {
                    bookmarkings[key] = req.body[key];
                }
            }

            bookmarkings.save(function(err) {
                if (err) {
                    return next(err);
                }

                res.json(bookmarkings);
            });
        });
    });

    router.delete('/:_id', function(req, res, next) {
        Bookmarkings.remove({
            _id: req.params._id
        }, function(err, bookmarkings) {
            if (err) {
                return next(err);
            }

            res.json({message: 'Successfully deleted'});
        });
    });

    return router;
})();
