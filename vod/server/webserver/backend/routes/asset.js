'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Asset = require('../models/asset'),
        shortId = require('short-mongo-id');

    router.get('/', function(req, res, next) {
        var params = {};
        if (req.query.title && req.query.tags) {
            Asset.find({title: {$regex: req.query.title}, tags: {$regex: req.query.tags}})
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }

                    res.json(asset);
                });
        } else if (req.query.title && !req.query.tags) {
            Asset.find({title: {$regex: req.query.title}})
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }

                    res.json(asset);
                });
        } else if (req.query.tags && !req.query.title) {
            Asset.find({tags: {$regex: req.query.tags}})
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }

                    res.json(asset);
                });
        } else if (req.query.actor) {
            Asset.find({actor: {$in: [req.query.actor]}})
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }

                    res.json(asset);
                });
        } else {
            Asset.find(params)
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }

                    res.json(asset);
                });
        }
    });

    router.get('/p', function(req, res) {
        Asset.find({tags: {$regex: req.query.tags}}, function(err, asset) {
            if (err) {
                return next(err);
            }

            res.json(asset);
        });
    });

    router.post('/', function(req, res, next) {
        var asset = new Asset();
        asset.id = shortId(asset._id);
        for (var key in req.body) {
            if(req.body.hasOwnProperty(key)) {
                asset[key] = req.body[key];
            }
        }

        asset.save(function(err) {
            if (err) {
                return next(err);
            }

            res.json(asset);
        });
    });

    router.get('/:input', function(req, res, next) {
        if(req.params.input == 'series') {
            Asset.find({type: 'series'}, function(err, asset) {
                if (err) {
                    return next(err);
                }

                res.json(asset);
            });
        } else if (req.params.input.length == 6) {
            Asset.findOne({id: req.params.input})
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }
                    res.json(asset);
                });
        }
        if (req.params.input.length == 24) {
            Asset.findById(req.params.input)
                .populate('items actor')
                .exec(function(err, asset) {
                    if (err) {
                        return next(err);
                    }
                    res.json(asset);
                });
        }
    });


    router.put('/:_id', function(req, res, next) {
        Asset.findById(req.params._id, function(err, asset) {
            if (err) {
                return next(err);
            }
            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)) {
                    asset[key] = req.body[key];
                }
            }
            asset.save(function(err) {
                if (err) {
                    return next(err);
                }
                res.json(asset);
            });
        });
    });

    router.delete('/:_id', function(req, res, next) {
        Asset.remove({
            _id: req.params._id
        },
                     function(err, asset) {
                         if (err) {
                             return next(err);
                         }
                         res.json({message: 'Successfully deleted'});
                     });
    });

    return router;
})();
