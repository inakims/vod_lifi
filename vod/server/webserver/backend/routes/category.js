'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Category = require('../models/category');
    router.get('/', function(req, res, next) {
        var params = {};

        if(req.query.mac) {
            params.mac = req.query.mac.toLowerCase();
        }

        Category.find(params)
            .populate('assets')
            .exec(function(err, category) {
                if (err) {
                    return next(err);
                }

                res.json(category);
            });
    });


    router.post('/', function(req, res, next) {
        var category = new Category();
        category.title = req.body.title || '-';
        category.assets = req.body.assets || [];
        category.type = req.body.assets || '-';

        category.save(function(err) {
            if (err) {
                return next(err);
            }

            Category.findOne({_id: category._id})
                .populate('questions')
                .exec(function(error, category) {
                    res.json(category);
                });
        });
    });


    router.get('/:_id', function(req, res, next) {
        Category.findById(req.params._id)
            .populate('assets')
            .exec(function(err, category) {
                if (err) {
                    return next(err);
                }

                res.json(category);
            });
    });


    router.put('/:_id', function(req, res, next) {
        Category.findById(req.params._id, function(err, category) {
            if (err) {
                return next(err);
            }

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)) {
                    category[key] = req.body[key];
                }
            }

            category.save(function(err) {
                if (err) {
                    return next(err);
                }

                res.json(category);
            });
        });
    });


    router.delete('/:_id', function(req, res, next) {
        Category.remove({
            _id: req.params._id
        }, function(err, category) {
            if (err) {
                return next(err);
            }

            res.json({message: 'Successfully deleted'});
        });
    });

    return router;
})();
