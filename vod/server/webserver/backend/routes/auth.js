'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        User = require('../models/users'),
        jwt = require('jsonwebtoken'),
        USER_NOT_FOUND = 'Authentication failed. User not found.',
        WRONG_PASSWORD = 'Authentication failed. Wrong password.',
        EXPIRATION_TOKEN = 1440;


    router.post('/authenticate', function(req, res) {
        User.findOne({
            username: req.body.username
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.status(400);
                res.json({message: USER_NOT_FOUND});
            } else if (user) {
                if (user.password != req.body.password) {
                    res.status(400);
                    res.json({message: WRONG_PASSWORD});
                } else {
                    console.log(req.app);
                    // create a token
                    var token = jwt.sign(user, 'secretKey', {
                        expiresIn: EXPIRATION_TOKEN
                    });
                    console.log(token);
                    res.json({
                        token: token
                    });
                }
            }
        });
    });

    router.get('/', function(req, res) {
        res.json({message: 'Welcome to the coolest API on earth!'});
    });

    router.get('/users', function(req, res) {
        User.find({}, function(err, users) {
            res.json(users);
        });
    });

    return router;
})();
