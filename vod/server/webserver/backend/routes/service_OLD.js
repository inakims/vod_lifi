'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Servers = require('../models/servers');

    router.get('/edges', function(req, res, next) {
        Servers.find().limit(3).exec(function(err, servers) {
            if (err) {
                return console.log(`get /edges ERROR: ${err}`);
            };
            console.log(servers);
            res.json(servers);
        });
    });

    /**
     * [initalizeServers description]
     */
    function initalizeServers() {
        Servers.find().exec(function(err, servers) {
            if (err) {
                return console.log(`initalizeServers ERROR: ${err}`);
            };

            if (!servers.length) {
                console.log('No servers --> Needed to initialize  servers');
                var initServers = [{
                    'name': 'cdn1',
                    'ip': '192.168.1.163',//'ip': '192.168.0.128',
                    'port': 8000
                },
                {
                    'name': 'cdn2',
                    'ip': '192.168.1.162',
                    'port': 8000
                },
                {
                    'name': 'cdn3',
                    'ip': '192.168.1.161',
                    'port': 8000
                }];

                var currServer;
                var newServer;
                var i;

                for (i = 0; i < initServers.length; i++) {
                    currServer = initServers[i];
                    newServer = new Servers({
                        name: currServer.name,
                        ip_address: currServer.ip,
                        port: currServer.port
                    });

                    newServer.save(function(err) {
                        if (err) {
                            console.log(`initServers SAVE ERROR: ${err}`);
                        }
                    });
                }
            }
        });
    }

    initalizeServers();
    return router;
})();
