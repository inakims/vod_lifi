'use strict';

module.exports = (function() {
    var router = require('express').Router(),
        Actor = require('../models/actor');

    router.get('/', function(req, res) {
        var params = {};

        Actor.find(params)
            .populate('acting')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }

                res.json(aux);
            });
    });

    router.get('/p', function(req, res) {
        Actor.find({name: {$regex: req.query.name}}, function(err, actor) {
            if (err) {
                return next(err);
            }

            res.json(actor);
        });
    });

    router.post('/', function(req, res, next) {
        var actor = new Actor();

        for(var key in req.body) {
            if(req.body.hasOwnProperty(key)) {
                actor[key] = req.body[key];
            }
        }

        actor.save(function(err) {
            if (err) {
                return next(err);
            }
            res.json(actor);
        });
    });

    router.get('/:_id', function(req, res, next) {
        Actor.findById(req.params._id)
            .populate('acting')
            .exec(function(err, aux) {
                if (err) {
                    return next(err);
                }

                res.json(aux);
            });
    });


    router.put('/:_id', function(req, res, next) {
        Actor.findById(req.params._id, function(err, actor) {
            if (err) {
                return next(err);
            }

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)) {
                    actor[key] = req.body[key];
                }
            }
            actor.save(function(err) {
                if (err) {
                    return next(err);
                }

                res.json(actor);
            });
        });
    });

    router.delete('/:_id', function(req, res, next) {
        Actor.remove({
            _id: req.params._id
        }, function(err, actor) {
            if (err) {
                return next(err);
            }

            res.json({message: 'Successfully deleted'});
        });
    });

    return router;
})();
