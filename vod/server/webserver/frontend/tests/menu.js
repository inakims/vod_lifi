'use strict';

describe('Menu Test', function() {
    var $scope,
        $compile,
        $templateCache,
        keycodes,
        deferred,
        controller,
        mocks,
        LIST_CONTAINER_WIDTH = 1032,
        LIST_CONTAINER_LEFT = 207,
        LIST_LEFT = -36,
        EXIT_BUTTON = {
            'Title': 'Salir',
            'Type': 'EXIT'
        };

    function move(key) {
        var onKeyDown = new window.KeyboardEvent('keydown', {
            bubbles: true,
            cancelable: true,
            shiftKey: true
        });

        delete onKeyDown.keyCode;
        Object.defineProperty(onKeyDown, 'keyCode', {'value': key});

        document.dispatchEvent(onKeyDown);
    }

    beforeEach(module('templates'));
    beforeEach(module('app'));
    beforeEach(module('app.menu'));

    beforeEach(inject(function($controller, $rootScope, _$q_, _$templateCache_, _$compile_, placerTvService, MockMenu, _keyCodes_) {
        $templateCache = _$templateCache_;
        $compile = _$compile_;
        keycodes = _keyCodes_;
        $scope = $rootScope.$new();

        // We use the $q service to create a mock instance of defer
        deferred = _$q_.defer();

        // Use a Jasmine Spy to return the deferred promise
        spyOn(placerTvService, 'getMenu').and.returnValue(deferred.promise);

        // Init the controller, passing our spy service instance
        controller = $controller('menuCtrl', {
            '$scope': $scope,
            'placerTvService': placerTvService
        });

        mocks = MockMenu;
    }));

    it('Menu initialized', function() {
        expect(controller.listConf).toBeDefined();
        expect(controller.listP).toBeDefined();
        expect(controller.lineStyle).toBeDefined();
        expect(controller.listStyle).toBeDefined();
        expect(controller.categories).toBeDefined();
    });

    it('Get Menu Categories OK', function() {
        controller.updateMenu(mocks.ITEMS);
        $scope.$apply();

        expect(controller.categories.length).toEqual(mocks.ITEMS.length);
        expect(controller.categories).toEqual(mocks.ITEMS);
    });

    it('EXIT button has been added', function() {
        controller.updateMenu(mocks.ITEMS);
        $scope.$apply();

        expect(controller.categories[controller.categories.length -1]).toEqual(EXIT_BUTTON);
    });

    it('GetItemwidth func', function() {
        var itemsNum = 0, itemsWidth = 0;

        controller.updateMenu(mocks.ITEMS);
        $scope.$apply();

        expect(controller.listConf.item.width).toBeDefined();
        expect(controller.categories).toBeDefined();

        itemsNum = controller.categories.length;
        itemsNum = itemsNum < 5 ? itemsNum : 5;

        itemsWidth = LIST_CONTAINER_WIDTH / itemsNum;

        expect(controller.listConf.item.width).toEqual(itemsWidth);
    });

    it('List initialization', function() {
        var listWidth = 0;

        deferred.resolve(mocks.ITEMS);
        $scope.$apply();

        expect(controller.listStyle).toBeDefined();

        listWidth = controller.listConf.item.width * controller.categories.length + 50;

        for(var key in controller.listStyle) {
            if(key === 'width') {
                expect(key).toContain('width');
                expect(controller.listStyle[key]).toEqual(listWidth + 'px');
            }else{
                expect(key).toContain('transform');
                expect(controller.listStyle[key]).toEqual('translate(' + LIST_LEFT + 'px,0px)');
            }
        }
    });

    it('Line initialization', function() {
        deferred.resolve(mocks.ITEMS);
        $scope.$apply();

        expect(controller.lineStyle).toBeDefined();

        for(var key in controller.lineStyle) {
            if(key === 'width') {
                expect(key).toContain('width');
                expect(controller.lineStyle[key]).toEqual(controller.listConf.item.width + 'px');
            }else{
                expect(key).toContain('transform');
                expect(controller.lineStyle[key]).toEqual('translate(' + LIST_CONTAINER_LEFT + 'px,0px)');
            }
        }
    });

    describe('Menu Navigation', function() {
        /*
        beforeEach(function() {
            var itemTmpl = $templateCache.get('app/shared/templates/menuItem.html'),
                listTmpl = $templateCache.get('app/components/menu/menu.html');

            $compile(itemTmpl)($scope);
            $scope.$digest();

            $compile(listTmpl)($scope);
            $scope.$digest();
        });

        it('EXIT Button reachable', function() {
            deferred.resolve(mocks.ITEMS);
            $scope.$apply();

            expect(controller.listP.getSelectedIndex()).toBe(0);
            move(keycodes.RIGHT);
            expect(controller.listP.getSelectedIndex()).not.toBe(0);

            for(var i=0; i<controller.categories.length - 1; i+=1) {
                move(keycodes.RIGHT);
            }

            expect(controller.listP.getSelectedIndex()).toBe(controller.categories.length -1);
            expect(controller.categories[controller.categories.length -1].Type).toEqual(EXIT_BUTTON.Type);
        });

        it('Non Circular list', function() {
            deferred.resolve(mocks.ITEMS);
            $scope.$apply();

            expect(controller.listP.getSelectedIndex()).toBe(0);
            move(keycodes.LEFT);
            expect(controller.listP.getSelectedIndex()).toBe(0);

            for(var i=0; i<controller.categories.length; i+=1) {
                move(keycodes.RIGHT);
            }

            expect(controller.listP.getSelectedIndex()).toBe(controller.categories.length -1);
            expect(controller.categories[controller.categories.length -1].Type).toEqual(EXIT_BUTTON.Type);

            move(keycodes.RIGHT);

            expect(controller.listP.getSelectedIndex()).toBe(controller.categories.length -1);
            expect(controller.categories[controller.categories.length -1].Type).toEqual(EXIT_BUTTON.Type);
        });

        it('Anim line movement', function() {
            var transition = false, translate = false;

            deferred.resolve(mocks.ITEMS);
            $scope.$apply();

            move(keycodes.RIGHT);

            for(var key in controller.lineStyle) {
                if(key === 'width') {
                    expect(key).toContain('width');
                    expect(controller.lineStyle[key]).toEqual(controller.listConf.item.width + 'px');
                }else{
                    if(key.indexOf('transition') !== -1) {
                        transition = true;
                        expect(controller.lineStyle[key]).toContain('transform');
                    }else if(key.indexOf('transform') !== -1) {
                        translate = true;
                        expect(controller.lineStyle[key]).not.toEqual('translate(' + LIST_CONTAINER_LEFT + 'px)');
                    }
                }
            }

            expect(transition).toBe(true);
            expect(translate).toBe(true);
        });

        it('Not Anim list movement for list of size smaller than 5', function() {
            var listWidth = 0;

            deferred.resolve(mocks.ITEMS);
            $scope.$apply();

            move(keycodes.RIGHT);

            listWidth = controller.listConf.item.width * controller.categories.length + 50;

            for(var key in controller.lineStyle) {
                if(key === 'width') {
                    expect(controller.listStyle[key]).toEqual(listWidth + 'px');
                }else if(key.indexOf('transform') !== -1) {
                    expect(controller.listStyle[key]).toEqual('translate(' + LIST_LEFT + 'px)');
                }
            }
        });
        */
    });
});
