'use strict';

describe('PlacerTV Service', function() {
    var STATUS = {
            OK: 'OK',
            NOK: 'NOK',
            ERROR: 'ERROR'
        },
        IdRegExp = /[\d\w-_]+$/.toString().slice(1, -1),
        service = null,
        mocks = null,
        backend = null,
        config = null,
        regexEscape = function regEsc(str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
        };

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, appConfig, placerTvService, mockFactory) {
        service = placerTvService;
        mocks = mockFactory;
        backend = $httpBackend;
        config = appConfig;
    }));

    it('Get Menu OK', function() {
        backend.whenGET(new RegExp(regexEscape(config.backend + 'Login/') + IdRegExp)).respond(function(method, url, data, headers) {
            return [200, mocks.login(STATUS.OK), {}];
        });

        backend.whenGET(config.backend + 'GetMenu').respond(function(method, url, data, headers) {
            return [200, mocks.getMenu(STATUS.OK), {}];
        });

        service.getMenu();
        // backend.flush();

        expect(service.menu).toBeDefined();
        expect(service.items).toBeDefined();
        // expect(service.menu).toEqual(mocks.getMenu(STATUS.OK).Items);
        expect(service.items).toEqual({});
    });

    it('Get Menu NOK', function() {
        backend.whenGET(new RegExp(regexEscape(config.backend + 'Login/') + IdRegExp)).respond(function(method, url, data, headers) {
            return [200, mocks.login(STATUS.OK), {}];
        });

        backend.whenGET(config.backend + 'GetMenu').respond(function(method, url, data, headers) {
            return [200, mocks.getMenu(STATUS.NOK), {}];
        });

        service.getMenu();
        // backend.flush();

        expect(service.menu).toBeDefined();
        expect(service.items).toBeDefined();
        expect(service.menu).toEqual([]);
        expect(service.items).toEqual({});
    });

    it('Get CategoryList OK', function() {
        var url = config.backend + 'CategoryList';
        backend.whenGET(new RegExp(regexEscape(config.backend + 'Login/') + IdRegExp)).respond(function(method, url, data, headers) {
            return [200, mocks.login(STATUS.OK), {}];
        });

        backend.whenGET(url).respond(function(method, url, data, headers) {
            return [200, mocks.getCategories(STATUS.OK), {}];
        });

        service.getCategories();
        // backend.flush();

        expect(service.menu).toBeDefined();
        expect(service.items).toBeDefined();
        expect(service.menu).toEqual([]);
        // expect(service.items[url].Assets).toBeDefined();
        // expect(service.items[url].ImagesWidth).toBeDefined();
        // expect(service.items[url].ImagesHeight).toBeDefined();
        // expect(service.items[url].TotalAssets).toBeDefined();
        // expect(service.items[url].TotalAssets).toEqual(service.items[url].Assets.length);
    });

    it('Get CategoryList NOK', function() {
        var url = config.backend + 'CategoryList';
        backend.whenGET(new RegExp(regexEscape(config.backend + 'Login/') + IdRegExp)).respond(function(method, url, data, headers) {
            return [200, mocks.login(STATUS.OK), {}];
        });

        backend.whenGET(url).respond(function(method, url, data, headers) {
            return [200, mocks.getCategories(STATUS.NOK), {}];
        });

        service.getCategories();
        // backend.flush();

        expect(service.menu).toBeDefined();
        expect(service.items).toBeDefined();
        expect(service.menu).toEqual([]);
        expect(service.items).toEqual({});
    });
});
