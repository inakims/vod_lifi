'use strict';

angular.module('app')
    .constant('regexEscape', function regEsc(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    })
    .run(['appConfig', 'mockFactory', 'regexEscape', '$httpBackend', '$log',
        function(appConfig, mockFactory, regexEscape, $httpBackend, $log) {
            if(!appConfig.useMocks) {
                return;
            }

            var STATUS = {
                    OK: 'OK',
                    NOK: 'NOK',
                    ERROR: 'ERROR'
                },
                IdRegExp = /[\d\w-_]+$/.toString().slice(1, -1),
                IdRegExpParams = /[\w\-\.]+[^#?\s]+/.toString().slice(1, -1);

            // GET SubscriberUser/<id>
            $httpBackend.whenGET(new RegExp(regexEscape(appConfig.backend + 'SubscriberUser/') + IdRegExp))
            .respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to SubscriberUser', data);
                var id = url.match(new RegExp(IdRegExp))[0];
                if (!id || id === 'undefined' || id === 'null') {
                    return [405, mockFactory.subscribeUser(STATUS.ERROR), {/* headers*/}];
                }
                return [200, mockFactory.subscribeUser(STATUS.OK), {/* headers*/}];
            });

            // GET SubscriberValidation/<id>
            $httpBackend.whenGET(new RegExp(regexEscape(appConfig.backend + 'Login/') + IdRegExp))
            .respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to SubscriberValidation', data);
                var id = url.match(new RegExp(IdRegExp))[0];
                if (!id || id === 'undefined' || id === 'null') {
                    return [405, mockFactory.login(STATUS.ERROR), {/* headers*/}];
                }
                return [200, mockFactory.login(STATUS.OK), {/* headers*/}];
            });

            // GET Logout
            $httpBackend.whenGET(appConfig.backend + 'logout').respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to Logout', data);
                return [200, mockFactory.logout(), {/* headers*/}];
            });

            // GET GetMenu
            $httpBackend.whenGET(appConfig.backend + appConfig.path + 'category').respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to GetMenu', data);
                return [200, mockFactory.getMenu(STATUS.OK), {/* headers*/}];
            });
            // $httpBackend.whenGET(new RegExp(regexEscape(appConfig.backend + appConfig.url) + IdRegExp))
            // .respond(function(method, url, data, headers) {
            //     $log.log('Intercepted GET to GetMenu', data);
            //     return [200, mockFactory.getMenu(STATUS.OK), {/* headers*/}];
            // });

            // GET CategoryList/
            $httpBackend.whenGET(appConfig.backend + 'CategoryList').respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to CategoryList', data);
                return [200, mockFactory.getCategories(STATUS.OK), {/* headers*/}];
            });

            // GET AssetsByCategory/<id>
            $httpBackend.whenGET(new RegExp(regexEscape(appConfig.backend + 'AssetsByCategory/') + IdRegExpParams))
            .respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to AssetsByCategory', data);
                var id = url.split('/')[4],
                    category = mockFactory.getCategoryById(id),
                    from = url.split('/')[5],
                    to = url.split('/')[6];

                if (!category) {
                    return [405, mockFactory.getAssets(STATUS.ERROR, from, to.split('?')[0]), {/* headers*/}];
                }
                return [200, mockFactory.getAssets(STATUS.OK, from, to.split('?')[0]), {/* headers*/}];
            });

            // GET Asset/<id>
            $httpBackend.whenGET(new RegExp(regexEscape(appConfig.backend + 'Asset/') + IdRegExp))
            .respond(function(method, url, data, headers) {
                $log.log('Intercepted GET to Asset', data);

                var id = url.match(new RegExp(IdRegExp))[0],
                    asset = mockFactory.getAssetById(id);

                if (!asset) {
                    return [405, mockFactory.getAsset(STATUS.ERROR), {/* headers*/}];
                }
                return [200, mockFactory.getAsset(STATUS.OK), {/* headers*/}];
            });

            // For App requests
            $httpBackend.whenGET(/^app\/.*/).passThrough();
        }]);
