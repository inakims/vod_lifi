'use strict';

angular.module('app')

    .constant('MockAssets', {
        'OK': {
            'Status': 1,
            'Message': 'OK'
        },
        'NOK': {
            'Status': 2,
            'Message': 'FORBIDDEN'
        },
        'ERROR': {
            'Status': 2,
            'Message': 'NOT_ALLOWED'
        },
        'BASE': {
            'Title': 'Título Video',
            'Image': {
                'Url': 'http://fakeimg.pl/423x275/',
                'Width': 300,
                'Height': 200
            },
            'UrlPlay': 'http://static.videogular.com/assets/videos/videogular.mp4',
            'TotalRelatedVideos': 0,
            'RelatedVideos': [],
            'RelatedVideosImageWidth': 300,
            'RelatedVideosImageHeight': 200
        }
    });
