'use strict';

angular.module('app')

    .constant('MockUser', {
        'OK': {
            'Status': 1,
            'Message': 'OK'
        },
        'NOK': {
            'Status': 2,
            'Message': 'FORBIDDEN'
        },
        'ERROR': {
            'Status': 2,
            'Message': 'NOT_ALLOWED'
        }
    });
