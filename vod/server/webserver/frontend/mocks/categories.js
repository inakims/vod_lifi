'use strict';

angular.module('app')

    .constant('MockCategories', {
        'OK': {
            'Status': 1,
            'Message': 'OK'
        },
        'NOK': {
            'Status': 2,
            'Message': 'FORBIDDEN'
        },
        'ERROR': {
            'Status': 2,
            'Message': 'NOT_ALLOWED'
        },
        'BASE': {
            'Id': 0,
            'Title': 'Título Categoría',
            'Image': {
                'Url': 'http://fakeimg.pl/423x275/',
                'Width': 300,
                'Height': 200
            },
            'Assets': [],
            'NumAssetsCategoryReturn': 0,
            'TotalAssetsCategory': 100
        }
    });
