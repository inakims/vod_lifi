'use strict';

angular.module('app')

    .factory('mockFactory', ['MockMenu', 'MockCategories', 'MockAssets', 'MockUser', '$filter',
        function(MockMenu, MockCategories, MockAssets, MockUser, $filter) {
            var factory = {},
                NUM_CATEGORIES = 10,
                NUM_ASSETS = 150,
                NUM_ASSETS_CATEGORY = 20,
                NUM_TAGS = 20,
                DEFAULT_STATUS = 'OK';

            factory.categoryList = [];
            factory.assetList = [];

            /**
            * creates an asset info video.
            * @param {Object} num
            * @param {Boolean} related
            * @return {Object} asset
            */
            function getInfoVideo(num, related) {
                var assetBase = MockAssets.BASE,
                    relatedList = null,
                    response = {
                        Title: assetBase.Title + ' ' + num,
                        Image: assetBase.Image,
                        UrlPlay: assetBase.UrlPlay
                    };

                if (related) {
                    relatedList = getAssetList(false);
                    response.RelatedVideos = relatedList;
                    response.TotalRelatedVideos = relatedList.length;
                    response.RelatedVideosImageWidth = assetBase.RelatedVideosImageWidth;
                    response.RelatedVideosImageHeight = assetBase.RelatedVideosImageHeight;
                }

                return response;
            }

            /**
            * creates an array of NUM_ASSETS assets.
            * @param {Boolean} related
            * @return {Array} asset list
            */
            function getAssetList(related) {
                var list = [],
                    assetInfo = null,
                    i = 0;
                if (!factory.assetList.length || !related) {
                    for (i = 0; i < NUM_ASSETS; i++) {
                        assetInfo = getInfoVideo(i, related);
                        list.push({
                            Id: i,
                            Title: assetInfo.Title,
                            Image: assetInfo.Image.Url,
                            InfoVideo: assetInfo
                        });
                    }
                    if (related) {
                        factory.assetList = list;
                    }
                } else {
                    list = factory.assetList;
                }
                return list;
            }

            /**
            * creates an array of NUM_TAGS tags.
            * @return {Array} tag list
            */
            function getTags() {
                var tags = [], i=0;

                for (i = 0; i < NUM_TAGS; i++) {
                    tags.push({
                        Title: 'Tag ' + i,
                        TagId: i
                    });
                }

                return tags;
            }

            /**
            * creates an array of NUM_CATEGORIES categories.
            * @return {Array} categories list
            */
            function getCategoryList() {
                if (!factory.categoryList.length) {
                    var categoryBase = MockCategories.BASE,
                        assetList = getAssetList(true),
                        i = 0;

                    factory.categoryList = [];
                    for (i = 0; i < NUM_CATEGORIES; i++) {
                        factory.categoryList.push({
                            Id: i,
                            Title: categoryBase.Title + ' ' + i,
                            Image: categoryBase.Image.Url,
                            Assets: assetList.slice(0, NUM_ASSETS_CATEGORY),
                            NumAssetsCategoryReturn: NUM_ASSETS_CATEGORY,
                            TotalAssetsCategory: assetList.length
                        });
                    }
                }
                return factory.categoryList;
            }

            // factory.getMenu = function(status) {
            //     var response = MockMenu[status || DEFAULT_STATUS];

            //     if (!status || status === DEFAULT_STATUS) {
            //         response.Items = MockMenu.ITEMS;
            //         response.TotalItems = MockMenu.ITEMS.length;
            //     }
            //     return response;
            // };
            factory.getMenu = function(status) {
                var response = MockMenu[status || DEFAULT_STATUS];
                if (!status || status === DEFAULT_STATUS) {
                    response = MockMenu.ITEMS;
                }
                return response;
            };

            factory.getCategories = function(status) {
                var response = MockCategories[status || DEFAULT_STATUS],
                    categoryBase = MockCategories.BASE,
                    categories = [];

                if (!status || status === DEFAULT_STATUS) {
                    categories = getCategoryList();

                    response.Categories = categories;
                    response.TotalAssets = categories.length;
                    response.ImagesWidth = categoryBase.Image.Width;
                    response.ImagesHeight = categoryBase.Image.Height;
                }
                return response;
            };

            factory.getCategoryById = function(id) {
                if (id == null) {
                    return;
                }

                var result = $filter('filter')(getCategoryList(), {Id: id});

                return result && result.length ? result[0] : null;
            };

            factory.getAsset = function(status) {
                var response = MockAssets[status || DEFAULT_STATUS];

                if (!status || status === DEFAULT_STATUS) {
                    response.Asset = getInfoVideo(0, true);
                }
                return response;
            };

            factory.getAssets = function(status, from, num) {
                var response = MockAssets[status || DEFAULT_STATUS],
                    assetBase = MockAssets.BASE,
                    assets = [];

                if (!status || status === DEFAULT_STATUS) {
                    assets = getAssetList(true);

                    response.Assets = assets.slice(from, from + num);
                    response.Tags = getTags(),
                    response.NumAssetsResponse = num;
                    response.TotalAssets = assets.length;
                    response.ImagesWidth = assetBase.Image.Width;
                    response.ImagesHeight = assetBase.Image.Height;
                }

                return response;
            };

            factory.getAssetById = function(id) {
                if (id == null) {
                    return;
                }
                var result = $filter('filter')(getAssetList(true), {Id: id}, true);
                return result && result.length ? result[0] : null;
            };

            factory.subscribeUser = function(status) {
                return MockUser[status || DEFAULT_STATUS];
            };

            factory.login = function(status) {
                return MockUser[status || DEFAULT_STATUS];
            };

            factory.logout = function() {
                return MockUser.OK;
            };

            return factory;
        }]);
