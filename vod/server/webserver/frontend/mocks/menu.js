'use strict';

angular.module('app')

    .constant('MockMenu', {
        'OK': {
            'Status': 1,
            'Message': 'OK'
        },
        'NOK': {
            'Status': 2,
            'Message': 'FORBIDDEN'
        },
        'ERROR': {
            'Status': 2,
            'Message': 'NOT_ALLOWED'
        },
        'ITEMS': [
            {
                'title': 'Home',
                'acronym': 'H',
                'type': 'film',
                'assets': [
                    {
                        'title': 'title 1',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Joseluisjime Carmen Jesus',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 0,
                        'IMBDrating': 8
                    },
                    {
                        'title': 'title 2',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Accion Aventura',
                        'filmactor': 'Jose Perez,Carmen Jose ,Jesus Lopez',
                        'items': [],
                        'rate': 3,
                        'IMBDrating': 8
                    },
                    {
                        'title': 'title 3',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Accion ',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 13,
                        'IMBDrating': 8
                    },
                    {
                        'title': 'title 4',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Joseluisjime Carmen Jesus',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 18,
                        'IMBDrating': 4
                    },
                    {
                        'title': 'title 5',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Joseluisjime Carmen Jesus',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 18,
                        'IMBDrating': 5.5
                    },
                    {
                        'title': 'title 6',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Aventura',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 0,
                        'IMBDrating': 6
                    },
                    {
                        'title': 'title 7',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Accion Aventura',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 0
                    },
                    {
                        'title': 'title 8',
                        'img': 'http://fakeimg.pl/423x275/',
                        'tags': 'Accion',
                        'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                        'items': [],
                        'rate': 0
                    }
                ]
            },
            {
                'title': 'My Series',
                'acronym': 'MS',
                'assets': [
                    {
                        'title': 'title 1',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 2',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 3',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 4',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 5',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 6',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    }
                ]
            },
            {
                'title': 'Discover',
                'acronym': 'D',
                'assets': [
                    {
                        'title': 'title 1',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 2',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 3',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 4',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 5',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 6',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    }
                ]
            },
            {
                'title': 'Calendar',
                'acronym': 'C',
                'assets': [
                    {
                        'title': 'title 1',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 2',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 3',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 4',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 5',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    },
                    {
                        'title': 'title 6',
                        'img': 'http://fakeimg.pl/423x275/',
                        'items': []
                    }
                ]
            },
            {
                'title': 'Genres',
                'acronym': 'G',
                'type': 'category',
                'assets': [
                    {
                        'title': 'title 1',
                        'img': 'http://fakeimg.pl/423x275/',
                        'acronym': 'T1',
                        'type': 'category',
                        'items': [
                            {
                                'title': 'title 1',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Joseluisjime Carmen Jesus',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 0,
                                'IMBDrating': 8
                            },
                            {
                                'title': 'title 2',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Accion Aventura',
                                'filmactor': 'Jose Perez,Carmen Jose ,Jesus Lopez',
                                'items': [],
                                'rate': 3,
                                'IMBDrating': 8
                            },
                            {
                                'title': 'title 3',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Accion ',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 13,
                                'IMBDrating': 8
                            },
                            {
                                'title': 'title 4',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Joseluisjime Carmen Jesus',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 18,
                                'IMBDrating': 4
                            },
                            {
                                'title': 'title 5',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Joseluisjime Carmen Jesus',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 18,
                                'IMBDrating': 5.5
                            },
                            {
                                'title': 'title 6',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Aventura',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 0,
                                'IMBDrating': 6
                            },
                            {
                                'title': 'title 7',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Accion Aventura',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 0
                            },
                            {
                                'title': 'title 8',
                                'img': 'http://fakeimg.pl/423x275/',
                                'tags': 'Accion',
                                'filmactor': 'Jose Perez,Carmen Jose uhihiuhuiiuh 9uhui,Jesus Lopez',
                                'items': [],
                                'rate': 0
                            }
                        ]
                    },
                    {
                        'title': 'title 2',
                        'img': 'http://fakeimg.pl/423x275/',
                        'acronym': 'T2',
                        'items': [
                            '59817b3d73178b0010aba21e',
                            '59817f3f73178b0010aba250',
                            '59817f5373178b0010aba251',
                            '59817d4973178b0010aba22a',
                            '59817db173178b0010aba232',
                            '59817e1873178b0010aba23c',
                            '59817e9d73178b0010aba241',
                            '59817eaa73178b0010aba242',
                            '59806cf1cb387f0011b59a05',
                            '59817ff173178b0010aba25d',
                            '5981801873178b0010aba25e',
                            '598180ce73178b0010aba263',
                            '59806cd0cb387f0011b59a03',
                            '598181df73178b0010aba27b',
                            '597f1b250933a20010132c51',
                            '5981825473178b0010aba28c',
                            '598182a673178b0010aba297',
                            '5981810573178b0010aba269',
                            '5981814f73178b0010aba26f',
                            '598181d973178b0010aba27a',
                            '598181fb73178b0010aba27e',
                            '5981823e73178b0010aba288',
                            '598182c973178b0010aba29a',
                            '5981831d73178b0010aba29d',
                            '5982d61c7d0b3d001186e19c'
                        ]
                    },
                    {
                        'title': 'title 3',
                        'img': 'http://fakeimg.pl/423x275/',
                        'acronym': 'T3',
                        'items': [
                            '59817b3d73178b0010aba21e',
                            '59817f3f73178b0010aba250',
                            '59817f5373178b0010aba251',
                            '59817d4973178b0010aba22a',
                            '59817db173178b0010aba232',
                            '59817e1873178b0010aba23c',
                            '59817e9d73178b0010aba241',
                            '59817eaa73178b0010aba242',
                            '59806cf1cb387f0011b59a05',
                            '59817ff173178b0010aba25d',
                            '5981801873178b0010aba25e',
                            '598180ce73178b0010aba263',
                            '59806cd0cb387f0011b59a03',
                            '598181df73178b0010aba27b',
                            '597f1b250933a20010132c51',
                            '5981825473178b0010aba28c',
                            '598182a673178b0010aba297',
                            '5981810573178b0010aba269',
                            '5981814f73178b0010aba26f',
                            '598181d973178b0010aba27a',
                            '598181fb73178b0010aba27e',
                            '5981823e73178b0010aba288',
                            '598182c973178b0010aba29a',
                            '5981831d73178b0010aba29d',
                            '5982d61c7d0b3d001186e19c'
                        ]
                    },
                    {
                        'title': 'title 4',
                        'img': 'http://fakeimg.pl/423x275/',
                        'acronym': 'T4',
                        'items': [
                            '59817b3d73178b0010aba21e',
                            '59817f3f73178b0010aba250',
                            '59817f5373178b0010aba251',
                            '59817d4973178b0010aba22a',
                            '59817db173178b0010aba232',
                            '59817e1873178b0010aba23c',
                            '59817e9d73178b0010aba241',
                            '59817eaa73178b0010aba242',
                            '59806cf1cb387f0011b59a05',
                            '59817ff173178b0010aba25d',
                            '5981801873178b0010aba25e',
                            '598180ce73178b0010aba263',
                            '59806cd0cb387f0011b59a03',
                            '598181df73178b0010aba27b',
                            '597f1b250933a20010132c51',
                            '5981825473178b0010aba28c',
                            '598182a673178b0010aba297',
                            '5981810573178b0010aba269',
                            '5981814f73178b0010aba26f',
                            '598181d973178b0010aba27a',
                            '598181fb73178b0010aba27e',
                            '5981823e73178b0010aba288',
                            '598182c973178b0010aba29a',
                            '5981831d73178b0010aba29d',
                            '5982d61c7d0b3d001186e19c'
                        ]
                    },
                    {
                        'title': 'title 5',
                        'img': 'http://fakeimg.pl/423x275/',
                        'acronym': 'T5',
                        'items': [
                            '59817b3d73178b0010aba21e',
                            '59817f3f73178b0010aba250',
                            '59817f5373178b0010aba251',
                            '59817d4973178b0010aba22a',
                            '59817db173178b0010aba232',
                            '59817e1873178b0010aba23c',
                            '59817e9d73178b0010aba241',
                            '59817eaa73178b0010aba242',
                            '59806cf1cb387f0011b59a05',
                            '59817ff173178b0010aba25d',
                            '5981801873178b0010aba25e',
                            '598180ce73178b0010aba263',
                            '59806cd0cb387f0011b59a03',
                            '598181df73178b0010aba27b',
                            '597f1b250933a20010132c51',
                            '5981825473178b0010aba28c',
                            '598182a673178b0010aba297',
                            '5981810573178b0010aba269',
                            '5981814f73178b0010aba26f',
                            '598181d973178b0010aba27a',
                            '598181fb73178b0010aba27e',
                            '5981823e73178b0010aba288',
                            '598182c973178b0010aba29a',
                            '5981831d73178b0010aba29d',
                            '5982d61c7d0b3d001186e19c'
                        ]
                    },
                    {
                        'title': 'title 6',
                        'img': 'http://fakeimg.pl/423x275/',
                        'acronym': 'T6',
                        'items': [
                            '59817b3d73178b0010aba21e',
                            '59817f3f73178b0010aba250',
                            '59817f5373178b0010aba251',
                            '59817d4973178b0010aba22a',
                            '59817db173178b0010aba232',
                            '59817e1873178b0010aba23c',
                            '59817e9d73178b0010aba241',
                            '59817eaa73178b0010aba242',
                            '59806cf1cb387f0011b59a05',
                            '59817ff173178b0010aba25d',
                            '5981801873178b0010aba25e',
                            '598180ce73178b0010aba263',
                            '59806cd0cb387f0011b59a03',
                            '598181df73178b0010aba27b',
                            '597f1b250933a20010132c51',
                            '5981825473178b0010aba28c',
                            '598182a673178b0010aba297',
                            '5981810573178b0010aba269',
                            '5981814f73178b0010aba26f',
                            '598181d973178b0010aba27a',
                            '598181fb73178b0010aba27e',
                            '5981823e73178b0010aba288',
                            '598182c973178b0010aba29a',
                            '5981831d73178b0010aba29d',
                            '5982d61c7d0b3d001186e19c'
                        ]
                    }
                ]
            }
        ]


    });


