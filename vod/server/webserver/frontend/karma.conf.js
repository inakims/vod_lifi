module.exports = function(config) {
    config.set({

        basePath: './',

        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-translate/angular-translate.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/cryptojslib/rollups/sha256.js',
            'bower_components/ng-dialog/js/ngDialog.js',
            'bower_components/angular-sanitize/angular-sanitize.min.js',
            'bower_components/videogular/videogular.js',
            'bower_components/videogular-controls/vg-controls.js',
            'bower_components/videogular-overlay-play/vg-overlay-play.js',
            'bower_components/videogular-poster/vg-poster.js',
            'bower_components/videogular-buffering/vg-buffering.js',
            'app/app.modules.js',
            'app/app.routes.js',
            'app/app.config.js',
            'app/shared/**/*.js',
            'app/shared/config/**/*.js',
            'mocks/*.js',
            'app/components/**/*.js',
            'tests/*.js',
            'app/components/**/*.html',
            'app/shared/**/*.html'
        ],

        preprocessors: {
            'app/components/**/*.html': ['ng-html2js'],
            'app/shared/**/*.html': ['ng-html2js']
        },

        ngHtml2JsPreprocessor: {
            moduleName: 'templates'
        },

        autoWatch: true,

        reporters: ['spec'],

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
            'karma-spec-reporter',
            'karma-ng-html2js-preprocessor',
            'karma-chrome-launcher',
            'karma-jasmine'
        ]
    });
};
