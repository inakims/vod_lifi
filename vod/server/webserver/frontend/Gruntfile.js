/* eslint no-invalid-this: 0 */

'use strict';

(function(module) {
    module.exports = function(grunt) {
        if (!grunt.file.exists('.server.json')) {
            grunt.fail.warn('File .server.json doesn\'t exist in the current workspace');
        }

        require('time-grunt')(grunt);
        require('load-grunt-config')(grunt, {
            configPath: require('path').join(process.cwd(), 'grunt/config'),
            init: true,
            data: { // data passed into config.  Can use with <%= test %>
                pkg: grunt.file.readJSON('package.json'),
                meta: {
                    app: {
                        NAME: 'app_placer_tv'
                    },
                    dirs: {
                        SRC: 'app/',
                        CONFIG: 'app/shared/config/',
                        TESTS: 'spec/',
                        BOWER: 'bower_components/',
                        ASSETS: 'assets/',
                        MOCKS: 'mocks/',
                        DIST: 'dist/tmp/',
                        LIBS: 'dist/tmp/libs/',
                        STYLES: 'dist/tmp/styles/',
                        FONTS: 'dist/tmp/assets/fonts',
                        RELEASE: 'dist/release/',
                        REPORTS: 'dist/reports/'
                    },
                    files: {
                        SRC_JS: ['<%= meta.dirs.SRC %>**/*.js'],
                        ALL_JS: ['<%= meta.dirs.SRC %>**/*.js', '<%= meta.files.SPECS %=>**/*.js',
                            'mocks/**/*.js', 'Gruntfile.js', 'grunt/**/*.js'],
                        SPECS: '<%= meta.dirs.TESTS %>**/*spec.js',
                        COVERAGE: [
                            '<%= meta.files.SRC_JS %>'
                        ]
                    },
                    server: grunt.file.readJSON('.server.json'),
                    git: {
                        REMOTE: 'origin',
                        BRANCH: 'develop',
                        COMMIT_MSG: 'Awesome Bot updated app version to <%= pkg.version %>',
                        TAG_MSG: 'App v<%= pkg.version %>'
                    }
                }
            }
        });

        // Custom tasks
        grunt.loadTasks('grunt/tasks');

        grunt.registerTask('lint', 'Launch linting tools', function(_target) {
            var target = '';
            if (_target) {
                target = ':' + _target;
            }
            grunt.task.run(['eslint' + target]);
        });
    };
})(module);
