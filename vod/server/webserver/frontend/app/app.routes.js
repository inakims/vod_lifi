'use strict';

angular.module('app')

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    'menu': {
                        templateUrl: 'app/components/menu/menu.html',
                        controller: 'menuCtrl as vm'
                    }
                }
            })
            .state('app.setup', {
                url: '/setup',
                views: {
                    'mosaic@': {
                        templateUrl: 'app/components/setup/setup.html',
                        controller: 'setupCtrl as vm'
                    }
                }
            })
            .state('app.home', {
                url: '/home',
                views: {
                    'mosaic@': {
                        templateUrl: 'app/components/mosaic/mosaic.html',
                        controller: 'mosaicCtrl as vm'
                    }
                },
                params: {
                    index: null
                }
            })
            .state('app.extendedInfo', {
                url: '/extendedInfo',
                views: {
                    'mosaic@': {
                        templateUrl: 'app/components/extendedInfo/extendedInfo.html',
                        controller: 'extendedInfoCtrl as vm'
                    }
                },
                params: {
                    asset: null,
                    index: null,
                    start: null
                }
            })
            .state('app.pip', {
                url: '/pip',
                views: {
                    'mosaic@': {
                        templateUrl: 'app/components/pip/pip.html',
                        controller: 'pipCtrl as vm'
                    }
                },
                params: {
                    asset: null,
                    index: null,
                    start: null
                }
            });
    }])

    .run(['$state', function($state) {
        $state.transitionTo('app.setup');
    }]);
