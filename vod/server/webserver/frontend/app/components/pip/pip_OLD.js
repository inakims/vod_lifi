'use strict';

angular.module('app.pip')

    .controller('pipCtrl', ['$scope', '$sce', '$state', '$stateParams', '$timeout', 'IBCService', 'messageService', 'appConfig',
        function($scope, $sce, $state, $stateParams, $timeout, IBCService, messageService, appConfig) {
            var vm = this;

            vm.playerConfig = {
                autoPlay: true,
                autoHide: true,
                autoHideTime: 0,
                autoStart: true,
                speeds: [1, 4, 8, 16, 64],
                sources: [],
                plugins: {
                    poster: ''
                },
                onError: function() {
                    if ($state.current.name === 'app.pip') {
                        messageService.infoCode('PIP_URL');
                    }
                },
                onExit: function() {
                    // window.clearInterval(vm.videoTimeInt);
                    $state.transitionTo('app.extendedInfo', {
                        asset: $stateParams.asset
                    });
                }
            };

            
            // vm.playerP = {};
            vm.bottomIcons = ['EXIT', 'OK', 'BACK'];

            /**
             * Key handler method that handles key presses.
             * @method keyHandler
             * @param {String} key The key that was pressed.
             * key press wasn't handled.
             */
            vm.keyHandler = function(key) {
                if (key === 'BACK') {
                    vm.playerConfig.onExit();
                };
            };
            /*IBCService.getEdges()
            .then(window.alert(`Please, run one Q4S client for each server:\n${JSON.stringify(IBCService.edges, 0, 1)}`))
            */

            window.alert(`Please, run one Q4S client for each server:\n${JSON.stringify(IBCService.edges, 0, 1)}`)
            // updatePi($sta teParams.asset);
            // q4sPlayer.init($stateParams.asset, IBCService.edges);
            
            q4sPlayer.startPlayback($stateParams.asset, IBCService.edges);
                    }
    ]);
