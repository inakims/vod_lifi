'use strict';

var globalIndex = 0;

angular.module('app.pip')

    .controller('pipCtrl', ['$scope', '$sce', '$state', '$stateParams', '$timeout', 'IBCService', 'messageService', 'appConfig',
        function($scope, $sce, $state, $stateParams, $timeout, IBCService, messageService, appConfig) {
            //IÑAKI
            /*var videoToRequest = $stateParams.asset.title;
            videoToRequest = videoToRequest.replace(/\s/g, "_");
            //console.log("Iniciando video-> "+videoToRequest);  
            IBCService.requestVideo(videoToRequest);*/
           
            createNewPlayer(globalIndex);
            if(globalIndex == 0){
                checkState(1);
                checkError(1);
            } 
            
            $scope.$on("$destroy", function(){
                removeOldPlayer(globalIndex);
                globalIndex = 0;
            });
             //IÑAKI
             
        }
        
    ]);

    //IÑAKI
    function createNewPlayer(indexPlayer){       
        var ind = indexPlayer + 1;
        //console.log("New index="+ind);
        var playerId = 'vxg_media_player' + ind;
        //console.log("Player Id = "+playerId);
        var div = document.createElement('div');
        div.setAttribute("id", playerId);
        div.setAttribute("class", "vxgplayer");
        var runtimePlayers = document.getElementById('playerUDP');
        runtimePlayers.appendChild(div);
        vxgplayer(playerId, {
                url: '',
                width: '1000',
                height: '563',
                autoreconnect: 1,
                nmf_path: 'media_player.nmf',
                nmf_src: 'app/components/pip/vxgplayer-1.8.31/pnacl/Release/media_player.nmf',
                latency: 300000,
                autohide: 2,
                controls: true,
                avsync: true,
                connection_timeout: 5000,
                connection_udp: 1
        }).ready(function(){
            console.log(' =>ready Player'+playerId);
            vxgplayer(playerId).src('udp://127.0.0.1:1234');
            vxgplayer(playerId).avsync(true);
            vxgplayer(playerId).play();
            console.log(' <=ready Player'+playerId);
            if(ind > 1){
                //console.log("No soy primer player");
                checkState(ind);
                checkError(ind);
            }
        });
    }

    function removeOldPlayer(playerId){
        //console.log("Received player="+playerId);
        console.log("Removing Player"+playerId);
        var playerId = 'vxg_media_player' + playerId;
        vxgplayer(playerId).dispose();
        document.getElementById(playerId).remove();
    }

    function checkError(playerId){
        //console.log("Inicio comprobación error player="+playerId);
        var player = vxgplayer('vxg_media_player'+playerId);
        player.onError(function(player){
            var err = player.error();
            console.log("ERROR="+err);
            if(err == 1 || err == 2){
                createNewPlayer(playerId);
                removeOldPlayer(playerId);
            }
        });
    }

    function checkState(playerId){
        globalIndex = playerId;
        //console.log("Inicio comprobación estado player="+playerId);
        var player = vxgplayer('vxg_media_player'+playerId);
        player.onStateChange(function(readyState){
            switch(readyState){
                case 0:
                    console.log("PLAYER_STOPPED");
                    break
                case 1:
                    console.log("PLAYER_CONNECTING");
                    break
                case 2:
                    console.log("PLAYER_PLAYING");
                    break
            }
        });
    }
    //IÑAKI
    
