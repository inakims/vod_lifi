'use strict';

angular.module('app.mosaic')

	.controller('mosaicCtrl', ['$state', '$scope', '$stateParams', '$rootScope', 'IBCService', 'movement',
    function($state, $scope, $stateParams, $rootScope, IBCService, movement) {
        var vm = this,
            top = 55,
            offset = 208,
            repeated = false,
            previousIndex = null;

        vm.mosaicConf = {
            circular: false,
            focus: false,
            focusIndex: {
                row: 1,
                col: 0
            },
            cols: 3,
            rows: 4,
            animRows: 1,
            focusedRow: 1,
            onChange: animate,
            onSelect: showExtendedInfo,
            getDefAsset: function() {},
            item: {
                template: 'app/shared/templates/mosaicItem.html'
            }
        };

        vm.mosaicP = {};
        vm.mosaicOuterStyle = {};
        vm.mosaicStyle = {};
        vm.assets = [];
        vm.bottomIcons =[];

        /**
        * Open pip and related videos screen
        * @param {object} data
        * @param {boolean} startPlay
        * @return {boolean} handled
        */
        function showExtendedInfo(data, startPlay) {
            if(data.type == 'category') {
                IBCService.getAsset(data.id);

                var cat = IBCService.getSelectedCategory();
                cat.idx = vm.mosaicP.getIndexPosition();

                IBCService.addToStack(cat);
                vm.mosaicP.selectIndex({row: 1, col: 0});
            }else{
                IBCService.addToStack(data);
                $state.transitionTo('app.extendedInfo', {
                    asset: data,
                    index: vm.mosaicP.getIndexPosition(),
                    start: startPlay
                });
            }

            return true;
        }

        /**
        * Animates list data change
        * @param {object} item
        * @param {string} key
        * @param {number} idx
        * @param {boolean} repaint
        * @param {number} row
        */
        function animate(item, key, idx, repaint, row) {
            if (repaint) {
                top += (offset * (key === 'DOWN' ? 1 : -1));
                vm.mosaicStyle = angular.merge({},
                    movement.getTop(top),
                    movement.getTransform(0, -top, repeated ? 0.4 : 0.6)
                );
            }
        }

        /**
        * Set the category assets
        * @param {Object} data
        */
        function setAssets(data) {
            vm.assets = data ? (data.assets ? data.assets : data.items) : [];
            vm.bottomIcons = data.type==='category'? ['BACK', 'OK'] : ['EXIT', 'OK'];

            if (previousIndex) {
                $rootScope.$broadcast('focus', 'back');

                vm.mosaicP.selectIndex(previousIndex);

                vm.mosaicP.highlight();
                previousIndex = null;
            }else{
                vm.mosaicP.selectIndex({col: 0, row: 1});
            }
        }

        /**
        * Set new catalog assets
        * @param {Object} category
        */
        function setCategory(category) {
            if(category!=null) {
                if (angular.isDefined(category.assets) || angular.isDefined(category.items)) {
                    top = 0;
                    vm.mosaicOuterStyle = movement.getTop(-228);

                    vm.mosaicStyle = angular.merge({},
                        movement.getTop(0),
                        movement.getTransform(0, 0, 0)
                    );

                    var field = category.type==='category' ? 'items' : 'assets';

                    angular.forEach(category[field], function(value, key) {
                        if(value.img.search(IBCService.getUrlImg()) === -1) {
                            value.img = (IBCService.getUrlImg() + value.img);
                        }
                    });

                    setAssets(category);
                }
            }
        }

        /**
         * Key handler method that handles key presses.
         * @method keyHandler
         * @param {String} key The key that was pressed.
         * @param {boolean} repeat
         * @return {Boolean} True if the key press was handled, false if the
         * key press wasn't handled.
         */
        vm.keyHandler = function(key, repeat) {
            var handled = false, stack = null;
            repeated = repeat;

            if (vm.mosaicP.isFocused()) {
                handled = vm.mosaicP.keyHandler(key);
            }

            if (!handled) {
                switch (key) {
                case 'UP':
                    if (vm.mosaicP.isFocused()) {
                        vm.mosaicP.unHighlight();
                    }
                    break;
                case 'DOWN':
                    if (!vm.mosaicP.isFocused()) {
                        if (vm.assets.length) {
                            vm.mosaicP.highlight();
                            vm.mosaicP.selectIndex({row: 1, col: 0});
                        } else {
                            handled = true;
                        }
                    }
                    break;
                case 'BACK':
                    stack = IBCService.getStack();
                    if(stack.length) {
                        previousIndex = stack[stack.length - 1].idx;
                        setCategory(IBCService.category);
                        IBCService.removeFromStack();
                    }

                    handled = true;
                    break;
                }
            }

            return handled;
        };

        previousIndex = $stateParams.index;

        $scope.$watch(function() {
            return IBCService.category;
        }, setCategory, true);

        $scope.$watch(function() {
            return IBCService.genre;
        }, setCategory, true);
    }]);
