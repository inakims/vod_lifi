'use strict';

angular.module('app.setup')

    .controller('setupCtrl', ['$state', 'IBCService', '$timeout',
        function($state, IBCService, $timeout) {
            var THRESHOLD = 1500,
                startedAt = new Date();

            /**
            * Show catalog and check splash visibility time
            */
            function showCatalog() {
                var elapsedTime = new Date().getTime() - startedAt.getTime();

                $timeout(function() {
                    $state.transitionTo('app.home');
                }, elapsedTime < THRESHOLD ? (THRESHOLD - elapsedTime) : 0);
            }

            /**
            * Load the menu categories
            */
            function loadMenu() {
                IBCService.getMenu()
                    .then(showCatalog);
                IBCService.getEdges();
            }

            loadMenu();
        }]);
