'use strict';

/**
 * Q4SPlayer constructor. It creates the DashJS stream and the websocket to
 * communicate with the Q4S connector
 */
function q4sPlayerObject() {
    // Crear instancia player de dashjs
    var q4sp = this;
    this.player = dashjs.MediaPlayer().create();
    this.serversAvailable = [];
    // serversAvailable === servers + level
    this.player.getDebug().setLogToBrowserConsole(false);
    this.currentServer;

    this.doneTime;
    

    this.init = function(content, servers) {
        q4sp.asset = content;
        q4sp.currentNumber = 0;
        for (var n = 0; n < servers.length; n++) {
            q4sp.serversAvailable[n] = servers[n];
            // Se inicializan todas los niveles de las conexiones a 0
            q4sp.serversAvailable[n]['level'] = 0;
            q4sp.serversAvailable[n]['running'] = false;
            /*console.log(n);
            console.log(q4sp.serversAvailable[n]);*/
        }

        q4sp.ws = new WebSocket('ws://192.168.1.126:7000');
        q4sp.ws.onopen = function(event) {
            console.log('connection opened');
        };
    
        q4sp.ws.onmessage = function(event) {
            messageHandler(event);
        };
    };

    this.startPlayback = function(content, servers) {
        // Iniciar la reproducción con el mejor en el estado actual
        // var bestServ = getBestServer();
        q4sp.init(content, servers);
        q4sp.setPlayerSource(getBestServer());
    };

    /**
     * Función que se llamará desde el conector cuando se reciba una alerta.
     * Alerta -> updateLevel -> selectBestServer
     * @return {[type]} [description]
     */

    this.setPlayerSource = function(server) {
        // server = {'name': 'xxxx', 'url':'xxx.xxx.xxx.xxx'}

        // Variable para guardar el servidor desde el que se va a servir el contenido
        q4sp.currentServer = server;

         switch (q4sp.currentServer.ip_address){
            case q4sp.serversAvailable[0].ip_address:
                q4sp.serversAvailable[0].running = true;
                console.log("1");
            break;
            case q4sp.serversAvailable[1].ip_address:
                q4sp.serversAvailable[1].running = true;
                console.log("2");
            break;
            case q4sp.serversAvailable[2].ip_address:
                q4sp.serversAvailable[2].running = true;
                console.log("3");
            break;

        }  


        console.log("set player");
        console.log(server);
        var srcAvailable;
        var completePath = `http://${server.ip_address}:${server.port}/trailers/${q4sp.asset.id}/video_live.mpd`;
        try {
            q4sp.player.getSource();
            srcAvailable = true;
        } catch (err) {
            console.log(`no src attached to the player`);
            // console.log(`try getSource ERROR: ${err}`);
            srcAvailable = false;
        }
        console.log('srcAvailable = ' + srcAvailable);

        if (!srcAvailable) {
            // Carga inicial del contenido
            console.log(completePath);
            q4sp.player.initialize(document.querySelector('#videoPlayer'), completePath, true);
            q4sp.currentServer = server;
            var date = new Date();
            var min = date.getMinutes()*60;
            console.log ('minutes  =>' + min );   
            var sec = date.getSeconds();
            console.log ('date  =>' + sec );  
            q4sp.doneTime = min + sec ;
            console.log ('min+sec=>' + q4sp.doneTime );     
        } 
        else {
            console.log('set source => switch');
            q4sp.player.attachSource(completePath);
            q4sp.player.on(dashjs.MediaPlayer.events['CAN_PLAY'], seekToElapsedTime);         
        }
    };

    /**
     * [getBestServer description]
     * @return {[type]} [description]
     */
    function getBestServer() {

        var minPos = 0, minLevel=0;

        var n, i;
        var selServer;

        for (n = 0; n < q4sp.serversAvailable.length; n++){

            if (q4sp.serversAvailable[n].running){
                minLevel=q4sp.serversAvailable[n].level;
                q4sp.serversAvailable[n].running=false;
               // console.log ("test if");
                break;
            }
           // console.log ("test for " + n);
        }


   
        //console.log(JSON.stringify(q4sp.serversAvailable,0,1));
        for (n = 0; n < q4sp.serversAvailable.length; n++) {
            selServer = q4sp.serversAvailable[n];
            
            if (selServer.level <= minLevel) {
                minLevel = selServer.level;
                minPos = n;
            // } else if (selServer.level === minLevel) {
                // if (n < minPos) {
                // Debido a que se recorre el array en orden creciente, nunca
                // puede darse el caso de que minpos sea mayor que n. Se asigna
                // hacia atras
                // minPos = n;
            }

            // En cuanto sea cero el mínimo nivel no se debe seguir comparando.
            // Es el nivel menor.
            if (minLevel === 0) break;
        }
        q4sp.serversAvailable[minPos].running=true;
        console.log('Selected server:');
        console.log(q4sp.serversAvailable[minPos]);
        return q4sp.serversAvailable[minPos];
    }

    /**
     * [seekToElapsedTime description]
     */
    function seekToElapsedTime() {
        var date = new Date();
        console.log(' time 3213 => ' + (date.getMinutes()*60 + date.getSeconds()));
        q4sp.elapsedTime =   (date.getMinutes()*60 + date.getSeconds())- q4sp.doneTime ;
        console.log ('elaps =>' +  q4sp.elapsedTime);   
        q4sp.player.seek(q4sp.elapsedTime);
        q4sp.elapsedTime = 0;
        q4sp.player.off(dashjs.MediaPlayer.events['CAN_PLAY'], seekToElapsedTime);
    };

    this.switchBestServer = function() { // ToDo filtering for IP

        switch (q4sp.currentServer.ip_address){
            case q4sp.serversAvailable[0].ip_address:
                q4sp.serversAvailable[0].level = q4sp.serversAvailable[0].level + 1;
            break;
            case q4sp.serversAvailable[1].ip_address:

                q4sp.serversAvailable[1].level = q4sp.serversAvailable[1].level + 1;
            break;
            case q4sp.serversAvailable[2].ip_address:

                q4sp.serversAvailable[2].level = q4sp.serversAvailable[2].level + 1 ;
            break;

        }
        //for (n = 1; n < q4sp.serversAvailable.length; n++) {
        for (var n = 0; n < 3; n++) {
            console.log ("ip " + q4sp.serversAvailable[n].ip_address)
            console.log( "level =>" + q4sp.serversAvailable[n].level);
        }

        var bestServer = getBestServer();
        // Si es el mimsmo, no se hace ningún cambio. El mejor es el actual
        console.log("ip bestServer");
        console.log(bestServer);
        console.log("ip currentServer");
        console.log(q4sp.currentServer.ip_address);




        if (bestServer.ip_address !== q4sp.currentServer.ip_address) {
            console.log("ip distintas");
            q4sp.setPlayerSource(bestServer);
        }
        else{
            console.log("ip iguales no hago nada");
            //q4sp.setPlayerSource(q4sp.serversAvailable[2]);
        }
    };

    /**
     * [updateLevel description]
     * @param  {[type]} ip    [description]
     * @param  {[type]} level [description]
     */
    this.updateLevel = function(ip, level) {
        for (var n = 0; n < q4sp.serversAvailable.length; n++) {
            if (q4sp.serversAvailable[n].ip === ip) {
                q4sp.serversAvailable[n].level = level;
                break;
            };
        };
        console.log(q4sp.serversAvailable);
    };

    /**
     * [messageHandler description]
     * @param  {[type]} event [description]
     */
    function messageHandler(event) {
        // Crear procedimiento para actualizar nivel para la ip que envía el mensaje
        // message = {message: , src: }
        // Se identifica cada servidor por la IP de quien emite la alerta/recovery


        console.log('MESSAGE RECEIVED');

        var body = JSON.parse(event.data),
            msg = body.msg;
        if (msg === 'updatelevel') {
            q4sp.updateLevel(body.ip, body.level);
        } else if (msg === 'switchbest') {
            q4sp.switchBestServer();
        }

        // console.log(message);
        // if (q4sp.currentNumber === 2){
        // 	q4sp.currentNumber = 0;
        // } else {
        // 	q4sp.currentNumber++;
        // }
        // q4sp.setPlayerSource(q4sp.serversAvailable[q4sp.currentNumber]);

        /* updateLevel(message);
        if ((message === alert && message.ip != q4sp.currentServer.ip) ||
        	(message === recovery && message.ip === q4sp.currentServer, ip)) {
            return;
            //	SOLO se actualiza el nivel

            // Alerta desde otra IP que no es la utilizada.
            // Recovery desde la IP en la que se sirve contenido.
        } else {
            var bestServ = getBestServer();

            if (bestServ.ip === q4sp.currentServer) {
                return;
            } else {
            	q4sp.elapsedTime = q4sp.player.time();
            	q4sp.setPlayerSource(bestServ);
            }
        }*/
    }

    // this.play = function(src)
}

var q4sPlayer = new q4sPlayerObject();
