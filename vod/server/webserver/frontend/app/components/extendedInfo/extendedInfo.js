'use strict';

angular.module('app.extendedInfo')
//IÑAKI
//.controller('extendedInfoCtrl', ['$scope', '$sce', '$state', '$stateParams', '$translate', 'movement', 'IBCService', 'messageService',
.controller('extendedInfoCtrl', ['$scope', '$sce', '$state', '$stateParams', '$translate', 'movement', 'IBCService', 'messageService', '$timeout',
    //function($scope, $sce, $state, $stateParams, $translate, movement, IBCService, messageService) {
    function($scope, $sce, $state, $stateParams, $translate, movement, IBCService, messageService, $timeout) {
    //IÑAKI
        var vm = this,
            LIST_ITEM_HEIGHT = 148.5,
            shift = 0,
            listWidth = 0,
            LIST_LEFT = -36,
            MAX_ITEMS = 5,
            LIST_CONTAINER_WIDTH = 1230,
            LIST_CONTAINER_LEFT = 0,
            BACK_BUTTON = {
                'title': 'BACK_BUTTON',
                'FaIcon': 'fa-arrow-left',
                'Type': 'BACK_BUTTON'
            },
            PLAY = {
                'title': 'PLAY',
                'Type': 'PLAY'
            },
            TRAILER = {
                'title': 'TRAILER',
                'Type': 'MENU'
            },
            RATE_BUTTON = {
                'title': 'RATE_BUTTON',
                'Type': 'MENU'
            },
            INTERESTED = {
                'title': 'INTERESTED',
                'Type': 'MENU'
            };

        vm.stars = 5;

        vm.getStarsNumber = function(num) {
            return new Array(num);
        };

        vm.listConf = {
            circular: false,
            direction: 0,
            focus: true,
            onChange: moveFocus,
            onSelect: aplyAction,
            item: {
                template: 'app/shared/templates/menuItem.html'
            }
        };
        vm.bottomIcons = ['BACK', 'OK'];

        vm.relatedListConf = {
            focusIndex: 0,
            circular: false,
            direction: 1,
            focus: false,
            onChange: moveRelatedFocus,
            onSelect: showExtendedInfo,
            item: {
                landscape: true,
                template: 'app/shared/templates/relatedItem.html'
            }
        };

        vm.relatedListP = {};
        vm.relatedListStyle = {};
        vm.listPSubmenu = {};

        /**
        * Open extended info and related videos screen
        * @param {object} data
        * @param {boolean} startPlay
        * @return {boolean} handled
        */
        function showExtendedInfo(data) {
            updateExtended(data);
            return true;
        }

        /**
        * Set related content for selected asset
        * @param {object} asset
        */
        function setRelatedContent(asset) {
            IBCService.getRelated(asset.tags)
                .then(function(resp) {
                    angular.forEach(resp, function(value, key) {
                        if(value.img && value.img.search(IBCService.getUrlImg()) === -1) {
                            value.img = (IBCService.getUrlImg() + value.img);
                        }
                    });
                    var i=0, relatedArray=[];
                    for(i; i<resp.length; i++) {
                        if(resp[i].type==='movie' && resp[i].title!==vm.extendedInfo.title) {
                            relatedArray.push(resp[i]);
                        }
                    }
                    vm.related= relatedArray;
                });
        }

        /**
        * Updates extendedInfo content
        * @param {object} asset
        */
        function updateExtended(asset) {
            var content = asset || $stateParams.asset;
            var tmp;

            if(content==null) {
                tmp=IBCService.stack;
                vm.extendedInfo=tmp[tmp.length-1];
            }else{
                vm.extendedInfo = content;
            }
            $stateParams.asset=vm.extendedInfo;
            if(vm.extendedInfo!==null) {
                if(vm.extendedInfo.filmactor!==undefined) {
                    vm.actores= vm.extendedInfo.filmactor.split(',', 3);
                } else{
                    vm.actores=[];
                }
                vm.extendedInfo.rating = vm.extendedInfo.IMBDrating/2;
                if(vm.extendedInfo.tags!==null) {
                    vm.tags= vm.extendedInfo.tags.split(' ');
                    if(vm.tags[0]== '') {
                        vm.tags.splice(0, 1);
                    }
                } else{
                    vm.tags=[' '];
                    vm.extendedInfo.tags='Drama Action';
                }
                switch(vm.extendedInfo.rate) {
                case 0: vm.rateimg='./assets/img/RATED_G.png'; break;
                case 3: vm.rateimg='./assets/img/RATED_PG.png'; break;
                case 13: vm.rateimg='./assets/img/RATED_PG-13.png'; break;
                default: vm.rateimg='./assets/img/RATED_R.png'; break;
                }
                setRelatedContent(vm.extendedInfo);
            }
        }


        /**
        * Execute secundary menu actions
        * @param {object} item
        * @param {startPlay} startPlay
        * @return {boolean} ret
        */
        function aplyAction(item, startPlay) {
            var ret = false;
            switch(item.title) {
            case 'BACK_BUTTON':
                if(IBCService.category.title!='Genres') {
                    IBCService.genre=null;
                    window.history.back();
                }
                $state.transitionTo('app.home', {
                    index: $stateParams.index
                });
                ret = true;
                break;
            case 'PLAY':
                $state.transitionTo('app.pip', {
                    asset: $stateParams.asset
                });
                ret = true;
                break;
            case 'TRAILER':
                //IÑAKI
                var videoToRequest = $stateParams.asset.title;
                videoToRequest = videoToRequest.replace(/\s/g, "_");
                //console.log("Iniciando video-> "+videoToRequest);  
                IBCService.requestVideo(videoToRequest);
                var THRESHOLD = 6500,
                    startedAt = new Date();
                showPip(THRESHOLD,startedAt);
                
                ret = true;
                break;
                //IÑAKI
            case 'RATE_BUTTON':
                messageService.rate('Rate value', vm.extendedInfo.rating);
                break;
            }
            return ret;
        }

        function showPip(THRESHOLD,startedAt){

            document.getElementById('titleDiv').style.display = 'none';
            document.getElementById('extendedInfo').style.display = 'none';
            document.getElementById('extendedCategories').style.display = 'none';
            document.getElementById('banner').style.display = 'none';
            document.getElementById('starsDiv').style.display = 'none';
            document.getElementById('imdbDiv').style.display = 'none';
            document.getElementById('ratingDiv').style.display = 'none';
            document.getElementById('loading').style.display = 'block';

            $scope.percentage = 0;
  
            var countUp = function() {
                var elapsedTime = new Date().getTime() - startedAt.getTime();
                if(elapsedTime >= THRESHOLD){
                    $scope.percentage = 100;
                    $state.transitionTo('app.pip', {
                        asset: $stateParams.asset
                    });
                }
                else{
                    $scope.percentage = Math.round((elapsedTime*100)/THRESHOLD);
                    $timeout(countUp, 500);
                }
            }
            
            $timeout(countUp, 500);

        }

        /**
        * Move focus line and set selected category
        * @param {object} item
        * @param {string} key
        * @param {number} idx
        */
        function moveFocus(item, key, idx) {
            vm.listStyle = getMovement(-idx * (vm.listConf.item.width - shift) + LIST_LEFT, listWidth, true);
            vm.lineStyle = getMovement(LIST_CONTAINER_LEFT + idx * shift, vm.listConf.item.width, true);
        }

        /**
        * Move focus line and set selected category
        * @param {object} item
        * @param {string} key
        * @param {number} idx
        */
        function moveRelatedFocus(item, key, idx) {
            var time = key ? 0.3 : null, shift = LIST_ITEM_HEIGHT;
            if (idx === 0 || idx === 1) {
                vm.relatedListStyle = movement.getTransform(0, 0, time);
            } else {
                if(idx === vm.related.length -1) {
                    shift = LIST_ITEM_HEIGHT + 178;
                }

                vm.relatedListStyle = movement.getTransform(0, (-idx * LIST_ITEM_HEIGHT) + shift, time);
            }
        }

        /**
        * Estimate width for menu list items
        * @param {number} itemsNum of items
        * @return {number} width in px
        */
        function getItemwidth(itemsNum) {
            return LIST_CONTAINER_WIDTH / (itemsNum <= MAX_ITEMS ? itemsNum : MAX_ITEMS);
        }

        /**
        * Estimate px to move focus
        * @param {number} itemsNum of items
        * @return {number} width in px
        */
        function getShift(itemsNum) {
            return (LIST_CONTAINER_WIDTH - vm.listConf.item.width) / (itemsNum - 1);
        }

        /**
        * @param {array} categories
        * Load menu categories
        */
        function updateMenu(categories) {
            if (categories && categories.length) {
                vm.listConf.item.width = getItemwidth(categories.length);
                shift = getShift(categories.length);
                listWidth = vm.listConf.item.width * categories.length + 50;

                vm.listStyle = getMovement(LIST_LEFT, listWidth);
                vm.lineStyle = getMovement(LIST_CONTAINER_LEFT, vm.listConf.item.width);
                vm.categories = categories;
            }
        }

        /**
        * Build class to move list items
        * @param {number} left
        * @param {number} width
        * @param {boolean} animate
        * @return {object} style properties
        */
        function getMovement(left, width, animate) {
            var time = animate ? 0.2 : null;
            return angular.merge({},
                    movement.getWidth(width),
                    movement.getTransform(left, 0, time)
                );
        }

        /**
        * Check if the menu list is ready and focused
        * @return {Boolean} isFocused
        */
        function listFocused() {
            return vm.listPSubmenu.isFocused && vm.listPSubmenu.isFocused();
        };

        // /**
        // * Unfocus menu list
        // */
        // function unFocusList() {
        //     vm.listPSubmenu.unHighlight();
        //     vm.listConf.focus = false;
        // }

        // /**
        // * Focus menu list
        // */
        // function focusList() {
        //     vm.listPSubmenu.highlight();
        //     vm.listConf.focus = true;
        // }

        /**
        * Check if the related list is ready and focused
        * @return {Boolean} isFocused
        */
        vm.relatedListFocused = function() {
            return vm.relatedListP.isFocused && vm.relatedListP.isFocused();
        };

        /**
        * Unfocus related list
        */
        function unFocusRelatedList() {
            vm.relatedListP.unHighlight();
        }

        /**
        * Focus related list
        */
        function focusRelatedList() {
            vm.relatedListP.highlight();
        }

        vm.keyHandler = function(key) {
            var handled = false, i=0, stack=IBCService.getStack();
            if(vm.relatedListFocused()) {
                handled = vm.relatedListP.keyHandler(key);
            }else if (listFocused()) {
                handled = vm.listPSubmenu.keyHandler(key);
                if(vm.listPSubmenu.getSelectedItem().title == 'INTERESTED') {
                    handled = false;
                }
            }
            if(!handled) {
                switch (key) {
                case 'DOWN':
                    if(listFocused() && !vm.relatedListFocused()) {
                        vm.listPSubmenu.selectIndex(4);
                        focusRelatedList();
                    }
                    break;
                case 'UP':
                    if(vm.relatedListFocused()) {
                        unFocusRelatedList();
                        vm.listPSubmenu.selectIndex(3);
                    }
                    handled=true;
                    break;
                case 'BACK':
                    stack=IBCService.getStack();
                    if(IBCService.category.title!='Genres') {
                        IBCService.genre=null;
                        for(i=0; i<stack.length; i++) {
                            IBCService.removeFromStack(stack[i]);
                        }
                    }

                    $state.transitionTo('app.home', {
                        index: $stateParams.index
                    });
                    handled = true;
                    break;
                case 'RIGHT':
                    if(!vm.relatedListFocused()) {
                        focusRelatedList();
                    }
                    break;
                case 'LEFT':
                    if(vm.relatedListFocused()) {
                        unFocusRelatedList();
                        vm.listPSubmenu.selectIndex(3);
                    }

                    break;
                }
            }
            return handled;
        };

        vm.categories = [BACK_BUTTON, PLAY, TRAILER, RATE_BUTTON, INTERESTED];
        updateMenu(vm.categories);
        updateExtended();
    }
]);
