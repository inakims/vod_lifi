'use strict';

angular.module('app.menu')

    .controller('menuCtrl', ['$scope', '$state', '$timeout', 'IBCService', 'movement', 'tivoService',
        function($scope, $state, $timeout, IBCService, movement, tivoService) {
            var vm = this,
                shift = 0,
                listWidth = 0,
                selectedIndex = null,
                updateTout = null,
                LIST_LEFT = -36,
                LIST_CONTAINER_WIDTH = 1011,
                LIST_CONTAINER_LEFT = 207,
                MAX_ITEMS = 5,
                UPDATE_DELAY = tivoService.getBrowserConfig() === 'velazquez' ? 300 : 500;

            vm.listConf = {
                circular: false,
                direction: 0,
                focus: true,
                onChange: moveFocus,
                onSelect: function(item) {
                    if ($state.current.name !== 'app.home') {
                        $state.transitionTo('app.home');
                    }
                },
                item: {
                    template: 'app/shared/templates/menuItem.html'
                }
            };

            vm.listP = {};
            vm.lineStyle = {};
            vm.listStyle = {};
            vm.categories = [];

            /**
            * Select action for a category item
            * @param {object} item
            */
            function selectMenuItem(item) {
                IBCService.setSelectedCategory(item);
                selectedIndex = vm.listP.getSelectedIndex ? vm.listP.getSelectedIndex() : 0;
            }

            /**
            * Build class to move list items
            * @param {number} left
            * @param {number} width
            * @param {boolean} animate
            * @return {object} style properties
            */
            function getMovement(left, width, animate) {
                var time = animate ? 0.2 : null;
                return angular.merge({},
                    movement.getWidth(width),
                    movement.getTransform(left, 0, time)
                );
            }

            /**
            * Move focus line and set selected category
            * @param {object} item
            * @param {string} key
            * @param {number} idx
            */
            function moveFocus(item, key, idx) {
                vm.listStyle = getMovement(-idx * (vm.listConf.item.width - shift) + LIST_LEFT, listWidth, true);
                vm.lineStyle = getMovement(LIST_CONTAINER_LEFT + idx * shift, vm.listConf.item.width, true);

                if (key && $state.current.name !== 'app.pip') {
                    $timeout.cancel(updateTout);

                    updateTout = $timeout(function() {
                        selectMenuItem(item);
                    }, UPDATE_DELAY);
                }
            }

            /**
            * Estimate width for menu list items
            * @param {number} itemsNum of items
            * @return {number} width in px
            */
            function getItemwidth(itemsNum) {
                return LIST_CONTAINER_WIDTH / (itemsNum <= MAX_ITEMS ? itemsNum : MAX_ITEMS);
            }

            /**
            * Estimate px to move focus
            * @param {number} itemsNum of items
            * @return {number} width in px
            */
            function getShift(itemsNum) {
                return (LIST_CONTAINER_WIDTH - vm.listConf.item.width) / (itemsNum - 1);
            }

            /**
            * @param {array} categories
            * Load menu categories
            */
            vm.updateMenu = function(categories) {
                if (categories && categories.length) {
                    vm.listConf.item.width = getItemwidth(categories.length);
                    shift = getShift(categories.length);
                    listWidth = vm.listConf.item.width * categories.length + 50;

                    vm.listStyle = getMovement(LIST_LEFT, listWidth);
                    vm.lineStyle = getMovement(LIST_CONTAINER_LEFT, vm.listConf.item.width);
                    vm.categories = categories;

                    selectMenuItem(vm.categories[0]);
                }
            };

            /**
             * Key handler method that handles key presses.
             * @method keyHandler
             * @param {String} key The key that was pressed.
             * @return {Boolean} True if the key press was handled, false if the
             * key press wasn't handled.
             */
            vm.keyHandler = function(key) {
                var handled = false;
                if (vm.categories.length) {
                    if (vm.listP.isFocused()) {
                        handled = vm.listP.keyHandler(key);
                    }

                    if (!handled) {
                        switch (key) {
                        case 'UP':
                            if (!vm.listP.isFocused()) {
                                vm.listP.highlight();
                            }
                            break;
                        case 'DOWN':
                            if (vm.listP.isFocused()) {
                                vm.listP.unHighlight();
                            }
                            break;
                        }
                    }
                }
                return handled;
            };

            // Listener to focus the menu for the unsuccessful category
            // request when return from the pip to the mosaic.
            $scope.$on('focus', function(event, ctx) {
                switch(ctx) {
                case 'back':
                    vm.listP.unHighlight();
                    vm.listP.selectIndex(selectedIndex);
                    break;
                }
            });

            $scope.$watch(function() {
                return IBCService.menu;
            }, vm.updateMenu);
        }]);
