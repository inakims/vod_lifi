'use strict';

var modules = [
        'ui.router',
        'ngDialog',
        'pascalprecht.translate',
        'ngSanitize',
        /*'com.2fdevs.videogular',
        'com.2fdevs.videogular.plugins.controls',
        'com.2fdevs.videogular.plugins.overlayplay',
        'com.2fdevs.videogular.plugins.buffering',
        'com.2fdevs.videogular.plugins.poster',
        'com.2fdevs.videogular.plugins.dash',*/
        'app.setup',
        'app.menu',
        'app.mosaic',
        'app.pip',
        'app.extendedInfo'

    ],
    ENV = 'DEV';

if (ENV !== 'DEV') {
    modules.unshift('templates-main');
}

// modules.unshift('templates-main');
angular.module('app.setup', []);
angular.module('app.menu', []);
angular.module('app.mosaic', []);
angular.module('app.pip', []);
angular.module('app.extendedInfo', []);


angular.module('app', modules);
