'use strict';

var translations = {
    ADD_BUTTON: 'Add to watch list',
    BACK: 'Back',
    BACK_BUTTON: 'Return',
    BANNER_FULLSCREEN: 'Pantalla Completa',
    BANNER_PAUSE: 'Pausar',
    BANNER_PLAY: 'Reproducir',
    BANNER_STOP: 'Ver',
    BANNER_RETURN: 'Return',
    CANCEL: 'Cancelar',
    COD_ERROR: 'Cod Error ',
    COD_ERROR_SETUP: 'En este momento no podemos validar tu dispositivo.',
    COD_ERROR_MOSAIC: 'Contenido no disponible.',
    COD_ERROR_PIP: 'No ha sido posible cargar el vídeo seleccionado.',
    COD_ERROR_SERVICE: 'Servicio no disponible.',
    CONFIRM: 'Aceptar',
    CAT_NEXT: 'Siguiente',
    CAT_PREV: 'Anterior',
    CAT_ADD_FILTER: 'Filtrar',
    CAT_REMOVE_FILTER: 'Borrar filtros',
    CURRENT_FILTER: 'Filtro:',
    DEFAULT_ERROR_MESSAGE: 'Error desconocido',
    DEFAULT_ERROR_TITLE: 'ERROR',
    DEFAULT_INFO_MESSAGE: 'Información',
    DEFAULT_INFO_TITLE: 'INFO',
    EXIT: 'Exit',
    EXIT_BUTTON: 'Close',
    FILTER: 'Filtrar',
    INTERESTED: 'Recommended',
    LOADING: 'Cargando ...',
    MAX_FILTER: 'Número máximo de filtros aplicados superado',
    MAX_FILTER_MESSAGE: 'Elimine uno de los filtros para poder aplicar el filtro seleccionado.',
    OK: 'Ok',
    PIN_TITLE: 'Introducir PIN',
    PIN_TITLE_WRONG: 'PIN Incorrecto',
    PIN_MESSAGE: 'Este programa está bloqueado porque es para Mayores de 18. Introduce tu PIN de Control paterno para ver este programa. '
    + 'El Control paterno volverá a su configuración anterior automáticamente si nadie usa'+
    ' el mando durante 4 horas o cuando se apague tu equipo.',
    PIN_MESSAGE_WRONG: 'El PIN de Control paterno que has introducido es incorrecto. Por favor, inténtalo de nuevo o pulsa' +
    ' IZQUIERDA para volver.',
    PIN_MESSAGE_LOCK: 'Inserte el PIN de la aplicación para acceder.',
    PLAY: 'Play',
    RATE_BUTTON: 'Rate',
    RELATED_EMPTY: 'Contenido relacionado no disponible.',
    RELATED_CONTENT: 'Vídeos relacionados',
    SCROLL_UP: 'Volver arriba',
    SELECT: 'Select',
    TRAILER: 'Watch trailer',
    VERSION_MESSAGE: 'Versión: '
};

angular.module('app')

    .constant('appConfig', {
        //backend: 'http://192.168.1.107:5080/',
        backend: 'http://127.0.0.1:5080/',
        useMocks: false,
        fakeDelay: 2000,
        audiometry: true,
        pinSetup: ['TWO', 'FIVE', 'SEVEN', 'NINE'],
        codeVersion: ['THREE', 'SIX', 'EIGHT', 'ZERO'],
        appVersion: 'X.X.X',
        path: 'api/vod/',
        pathImg: 'img/'
    })
    .config(['$translateProvider', '$httpProvider', 'appConfig', '$provide',
        function($translateProvider, $httpProvider, appConfig, $provide) {
            $translateProvider.translations('es', translations);
            $translateProvider.preferredLanguage('es');
            $translateProvider.useSanitizeValueStrategy('escape');

            $httpProvider.interceptors.push('xhrInterceptor');
            $httpProvider.defaults.withCredentials = true; // cookies

            if (appConfig.useMocks) {
                $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
            }
        }])
    .config(['ngDialogProvider', function(ngDialogProvider) {
        ngDialogProvider.setOpenOnePerName(true);
        ngDialogProvider.setDefaults({
            className: '',
            plain: false,
            showClose: false,
            closeByDocument: false,
            closeByEscape: false,
            appendTo: false
        });
    }]);
