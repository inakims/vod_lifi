'use strict';

angular.module('app')

    .constant('keyCodes', {
        A: 'a',
        B: 'b',
        C: 'c',
        D: 'd',
        E: 'e',
        F: 'f',
        G: 'g',
        H: 'h',
        I: 'i',
        J: 'j',
        K: 'k',
        L: 'l',
        M: 'm',
        N: 'n',
        O: 'o',
        P: 'p',
        Q: 'q',
        R: 'r',
        S: 's',
        T: 't',
        U: 'u',
        V: 'v',
        W: 'w',
        X: 'x',
        Y: 'y',
        Z: 'z',
        ZERO: '0',
        ONE: '1',
        TWO: '2',
        THREE: '3',
        FOUR: '4',
        FIVE: '5',
        SIX: '6',
        SEVEN: '7',
        EIGHT: '8',
        NINE: '9',
        SPACE: ' ',
        BACKSPACE: 'Backspace',
        INFO: 'Insert', // 'Info'
        BACK: 'Escape', // 'Esc'
        EXIT: 'º', // 'Exit'
        TEXT: 'NumpadDivide', // 'Teletext'
        SUBTITLES: '*', // 'Subtitle'
        UP: 'ArrowUp', // 'Up'
        DOWN: 'ArrowDown', // 'Down',
        LEFT: 'ArrowLeft', // 'Left',
        RIGHT: 'ArrowRight', // 'Right',
        SELECT: 'Enter',
        CH_UP: 'NumpadAdd', // 'MediaTrackNext'
        CH_DOWN: 'NumpadSubtract', // MediaTrackPrevious'
        THUMBS_UP: 'NumpadMultiply', // 'StoreFavorite0'
        THUMBS_DOWN: 'NumpadDivide', // 'ClearFavorite0'
        PLAY: 'F1', // 'MediaPlay'
        PAUSE: 'F2', // 'MediaPause'
        PLAY_PAUSE: 'F3', // 'MediaStop'
        STOP: 'F4', // 'MediaStop'
        REWIND: 'F5', // 'MediaRewind'
        FAST_FORWARD: 'F6', // 'FastFwd'
        SLOW: 'F7', // 'PlaySpeedDown'
        SKIP_BACK: 'F8', // 'MediaTrackStart'
        SKIP_AHEAD: 'F9', // 'MediaTrackEnd'
        RED: 'Home', // 'Red'
        GREEN: 'End', // 'Green'
        YELLOW: 'PageUp', // 'Yellow'
        BLUE: 'PageDown', // 'Blue',
        CLEAR: 'IntlBackslash' // 'Clear'
    });
