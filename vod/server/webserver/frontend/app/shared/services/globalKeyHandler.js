'use strict';

angular.module('app')

    .factory('globalKeyHandler', ['messageService', function(messageService) {
        var service = {};

        service.keyHandlers = [];

        /**
         * Key handler method that handles key presses for the app.
         * @method appGlobalKeyHandler
         * @param {String} key The key that was pressed.
         * @param {Boolean} isRepeat
         * @param {Boolean} handled
         * @return {Boolean} True if the key press was handled, false if the
         * key press wasn't handled.
         */
        function appGlobalKeyHandler(key, isRepeat, handled) {
            if (!isRepeat) {
                if (!handled) {
                    switch (key) {
                    case 'ZERO':
                    case 'ONE':
                    case 'TWO':
                    case 'THREE':
                    case 'FOUR':
                    case 'FIVE':
                    case 'SIX':
                    case 'SEVEN':
                    case 'EIGHT':
                    case 'NINE':
                        messageService.codeMessagePressed(key);
                        handled = true;
                        break;
                    default:
                        messageService.resetCodeMessage();
                    }
                } else {
                    messageService.resetCodeMessage();
                }
            }
            return handled;
        }

        service.addKeyHandler = function(keyHandler) {
            service.keyHandlers.push(keyHandler);
        };

        service.removeKeyHandler = function(keyHandler) {
            service.keyHandlers.splice(service.keyHandlers.indexOf(keyHandler), 1);
        };

        service.excuteKeyHandlers = function(keyDown, isRepeat) {
            var ret = false,
                i = service.keyHandlers.length - 1;

            while (!ret && i >= 0) {
                ret = service.keyHandlers[i](keyDown, isRepeat);
                i--;
            }

            return appGlobalKeyHandler(keyDown, isRepeat, ret);
        };

        return service;
    }]);
