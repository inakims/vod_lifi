'use strict';

angular.module('app')

    .factory('audiometryService', ['$http', '$interval', '$filter', 'tivoService', 'appConfig',
        function($http, $interval, $filter, tivoService, appConfig) {
            var service = {},
                APP_NAME = 'PlacerTV',
                URL_BASE = 'http://62.42.230.137/audimetria_flash/aud.php',
                TYPE = {
                    INSERT: 'insert',
                    UPDATE: 'update'
                },
                UPDATE_TIME = 60000,
                initTime = null;

            /**
            * Returns the init timestamp
            * @return {string} initTime
            */
            function getTimestamp() {
                if (!initTime) {
                    initTime = $filter('date')(new Date(), 'yyyyMMddHHmm');
                }

                return initTime;
            }

            /**
            * Get the url for a type
            * @param {TYPE} type
            * @return {string} url
            */
            function getUrl(type) {
                return URL_BASE + '?tsn=' + tivoService.getId() + '&app=' + APP_NAME +
                    '&fecha=' + getTimestamp() + '&type=' + type;
            }

            service.request = function(url) {
                return $http.get(url).then(function(resp) {}, function(e) {});
            };

            service.insert = function(actionFn) {
                return service.request(getUrl(TYPE.INSERT));
            };

            service.update = function() {
                return service.request(getUrl(TYPE.UPDATE));
            };

            service.start = function() {
                if (appConfig.audiometry && !appConfig.useMocks) {
                    service.insert();
                    $interval(service.update, UPDATE_TIME);
                }
            };

            return service;
        }]);
