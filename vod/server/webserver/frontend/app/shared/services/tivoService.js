'use strict';

angular.module('app')

    .factory('tivoService', ['$window', function($window) {
        var service = {},
            configurationObj = ENV !== 'DEV' ? oipfObjectFactory.createConfigurationObject() : null,
            parentalControlObj = ENV !== 'DEV' ? oipfObjectFactory.createParentalControlManagerObject() : null,
            ua = $window.navigator.userAgent,
            CONFIG = {
                cisco: /Presto/i,
                velazquez: /AppleWebKit/i
            },
            DEFAULT_CONFIG = 'velazquez';

        service.getBrowserConfig = function() {
            var browser = service.getBrowserType(),
                key = null;

            for(key in CONFIG) {
                if (CONFIG[key].test(browser)) {
                    return key;
                }
            };
            return DEFAULT_CONFIG;
        };

        service.getBrowserType = function() {
            var uaPrams = ua.split('/');
            return uaPrams[1];
        };

        service.getId = function(actionFn) {
            return service.localSys.serialNumber;
        };

        service.exit = function() {
            tivo.core.exit();
        };

        service.isParentalEnabled = function() {
            var ret = false, pc = false;

            if(parentalControlObj) {
                pc = parentalControlObj.getParentalControlStatus();

                if(pc === 2) {
                    ret = true;
                }
            }

            return ret;
        };

        service.verifyParentalControlPIN = function(pin) {
            var ret = true, resp = false;

            if(parentalControlObj) {
                resp = parentalControlObj.verifyParentalControlPIN(pin);

                if(resp === 1) {
                    ret = false;
                }
            }

            return ret;
        };

        service.getAVInputType = function() {
            var ret = 'ETHERNET', i = 0;

            // FTTH -> ETHERNET
            // QUAM -> DOCSIS

            if(service.localSys.networkInterfaces) {
                for(i=0; i<service.localSys.networkInterfaces.length; i+=1) {
                    if(service.localSys.networkInterfaces[i].type === 'DOCSIS' && service.localSys.networkInterfaces[i].connected) {
                        ret = 'DOCSIS';
                        break;
                    }
                }
            }

            return ret;
        };

        if (configurationObj) {
            service.localSys = configurationObj.localSystem;
        } else {
            service.localSys = {
                serialNumber: '0',
                networkStatus: $window.navigator.onLine
            };
        }

        return service;
    }]);
