
'use strict';

angular.module('app')

    .factory('xhrInterceptor', ['$injector', '$q', '$timeout', 'appConfig', function($injector, $q, $timeout, appConfig) {
        var loaderTout = null,
            isOpen = false,
            THRESHOLD = 50,
            request = {},
            xhrInjector = {};

        /**
        * Check if the url in the config is a API url
        * @param {object} config Configuration object that was used to generate the request.
        * @return {Boolean} isUrl
        */
        function isApiUrl(config) {
            return config && config.url && config.url.indexOf(appConfig.backend) > -1;
        };

        /**
        * Show the loading popup
        * @param {object} config Configuration object that was used to generate the request.
        */
        function loadingTimeout(config) {
            if (config.loading) {
                clearTimeout(loaderTout);
                loaderTout = $timeout(function() {
                    if (!isOpen && request[config.url]) {
                        if (!config.hidden) {
                            $injector.get('messageService').loading();
                        }
                        isOpen = true;
                    }
                }, THRESHOLD);
            }
        };

        /**
        * Hide the loading popup
        * @param {object} config Configuration object that was used to generate the request.
        */
        function loadedTimeout(config) {
            if (config.loading) {
                clearTimeout(loaderTout);
                loaderTout = null;
                $timeout(function() {
                    if (!loaderTout) {
                        $injector.get('messageService').loaded();
                        isOpen = false;
                        if (request[config.url]) {
                            delete request[config.url];
                        }
                    }
                }, THRESHOLD * 2);
            }
        };

        xhrInjector = {
            request: function(config) {
                if (isApiUrl(config)) {
                    request[config.url] = true;
                    loadingTimeout(config);
                }

                return config;
            },
            response: function(resp) {
                if (isApiUrl(resp.config)) {
                    if (resp.config.delay || appConfig.useMocks) {
                        var deferred = $q.defer();
                        $timeout(function() {
                            loadedTimeout(resp.config);
                            deferred.resolve(resp);
                        }, resp.config.delay || appConfig.fakeDelay);

                        return deferred.promise;
                    } else {
                        loadedTimeout(resp.config);
                    }
                }

                return resp;
            },
            responseError: function(resp) {
                var errorHandled = null;

                if (isApiUrl(resp.config)) {
                    loadedTimeout(resp.config);

                    if (request[resp.config.url]) {
                        delete request[resp.config.url];
                    }
                }

                return errorHandled || $q.reject(resp);
            }
        };

        return xhrInjector;
    }]);
