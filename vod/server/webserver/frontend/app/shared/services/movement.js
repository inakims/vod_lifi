'use strict';

angular.module('app')

    .factory('movement', [function() {
        var service = {};

        service.getWidthOpacity = function(width, time) {
            var widthOpacity = 'width ' + time + 's' + ', opacity ' + time + 's',
                style = {
                    'width': width + 'px',
                    'opacity': width > 0 ? 255 : 0
                };
            if (time) {
                style['-webkit-transition'] = widthOpacity;
                style['transition'] = widthOpacity;
            }
            return style;
        };

        service.getTransform = function(x, y, time) {
            var translate = 'translate(' + x + 'px,' + y + 'px)',
                style = {
                    '-webkit-transform': translate,
                    'transform': translate
                };
            if (time) {
                style['-webkit-transition'] = '-webkit-transform ' + time + 's';
                style['transition'] = 'transform ' + time + 's';
            }
            return style;
        };

        service.getTop = function(pixels) {
            return {'top': pixels + 'px'};
        };

        service.getWidth = function(pixels) {
            return {'width': pixels + 'px'};
        };

        return service;
    }]);
