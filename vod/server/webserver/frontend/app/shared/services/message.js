'use strict';

angular.module('app')
    .controller('DialogController', ['$scope', 'ngDialog', function($scope, ngDialog) {
        var dialogData = $scope.ngDialogData,
            actions = {
                CONFIRM: function() {
                    $scope.closeThisDialog(true);
                },
                CANCEL: function() {
                    $scope.closeThisDialog(false);
                }
            };

        $scope.title = (dialogData.title) ? dialogData.title : '';
        $scope.message = (dialogData.message) ? dialogData.message : '';
        $scope.buttons = [];

        $scope.listConf = {
            circular: false,
            direction: 0,
            focus: true,
            onSelect: function(item) {
                item.action();
                return true;
            },
            item: {
                template: 'app/shared/templates/messageButtonItem.html'
            }
        };
        $scope.listP = {};
        $scope.listStyle = {};

        if (dialogData.buttons) {
            angular.forEach(dialogData.buttons, function(button) {
                $scope.buttons.push({
                    title: button.title,
                    action: actions[button.title]
                });
            });
        }

        /**
         * Key handler method that handles key presses.
         * @method keyHandler
         * @param {String} key The key that was pressed.
         * @return {Boolean} True if the key press was handled, false if the
         * key press wasn't handled.
         */
        $scope.keyHandler = function(key) {
            var handled = false;
            if ($scope.listP.isFocused()) {
                // Buttons List
                handled = $scope.listP.keyHandler(key);

                if (!handled) {
                    switch (key) {
                    case 'BACK':
                        $scope.closeThisDialog(false);
                        handled = true;
                        break;
                    default:
                        handled = true;
                    }
                }
            }
            return handled;
        };
    }])

    .controller('PinDialogController', ['$scope', 'ngDialog', '$timeout', 'tivoService', function($scope, ngDialog, $timeout, tivoService) {
        var dialogData = $scope.ngDialogData,
            i = 0;

        $scope.title = (dialogData.title) ? dialogData.title : '';
        $scope.message = (dialogData.message) ? dialogData.message : '';
        $scope.pinCode = [];
        $scope.chars = [];

        for (i; i < dialogData.numChars; i++) {
            $scope.chars.push(dialogData.char);
        }

        /**
         * Key handler method that handles key presses.
         * @method keyHandler
         * @param {String} key The key that was pressed.
         * @param {Boolean} isRepeat
         * @return {Boolean} True if the key press was handled, false if the
         * key press wasn't handled.
         */
        $scope.keyHandler = function(key, isRepeat) {
            if (!isRepeat) {
                switch (key) {
                case 'ZERO':
                case 'ONE':
                case 'TWO':
                case 'THREE':
                case 'FOUR':
                case 'FIVE':
                case 'SIX':
                case 'SEVEN':
                case 'EIGHT':
                case 'NINE':
                    $scope.pinCode.push(key);
                    $scope.chars[$scope.pinCode.length -1] = '*';

                    if ($scope.pinCode.length === dialogData.numChars) {
                        $timeout(function() {
                            $scope.closeThisDialog($scope.pinCode);
                        });
                    }
                    break;
                case 'LEFT':
                    if($scope.pinCode.length) {
                        $scope.chars[$scope.pinCode.length -1] = '';
                        $scope.pinCode.pop();
                    }else{
                        tivoService.exit();
                    }
                    break;
                }
            }
            return true;
        };
    }])

    .factory('messageService', ['ngDialog', 'appConfig', '$translate', 'errorCodes', '$timeout', 'tivoService', 'keyCodes',
        function(ngDialog, appConfig, $translate, errorCodes, $timeout, tivoService, keyCodes) {
            var service = {},
                keyPressHistory = [];

            service.buttons = {
                confirm: {
                    title: 'CONFIRM'
                },
                cancel: {
                    title: 'CANCEL'
                },
                rate_button: {
                    title: 'RATE_BUTTON'
                }
            };

            service.info = function(title, message) {
                var dialog = ngDialog.open({
                    name: 'info',
                    controller: 'DialogController',
                    template: 'app/shared/templates/infoMessage.html',
                    disableAnimation: (tivoService.getBrowserConfig() === 'cisco' ? true : false),
                    data: {
                        title: title || $translate.instant('DEFAULT_INFO_TITLE'),
                        message: message || $translate.instant('DEFAULT_INFO_MESSAGE'),
                        buttons: [service.buttons.confirm]
                    }
                });
                return dialog && dialog.closePromise;
            };

            service.rate = function(title, message, rate) {
                var dialog = ngDialog.open({
                    name: 'info',
                    controller: 'RatingController',
                    template: 'app/shared/templates/rating.html',
                    disableAnimation: (tivoService.getBrowserConfig() === 'cisco' ? true : false),
                    data: {
                        title: title || $translate.instant('DEFAULT_INFO_TITLE'),
                        message: message || $translate.instant('DEFAULT_INFO_MESSAGE'),
                        rating: rate,
                        buttons: [service.buttons.rate_button]
                    }
                });
                return dialog && dialog.closePromise;
            };

            service.infoCode = function(code, status) {
                var title = status ? $translate.instant('DEFAULT_ERROR_TITLE') + ' ' + status : null,
                    message = errorCodes.getCodeMessage(code);
                return service.info(title, message);
            };

            service.error = function(title, message) {
                var dialog = ngDialog.open({
                    name: 'error',
                    controller: 'DialogController',
                    template: 'app/shared/templates/errorMessage.html',
                    disableAnimation: (tivoService.getBrowserConfig() === 'cisco' ? true : false),
                    data: {
                        title: title || $translate.instant('DEFAULT_ERROR_TITLE'),
                        message: message || $translate.instant('DEFAULT_ERROR_MESSAGE'),
                        buttons: service.buttons
                    }
                });
                return dialog && dialog.closePromise;
            };

            service.errorCode = function(code, status) {
                var title = status ? $translate.instant('DEFAULT_ERROR_TITLE') + ' ' + status : null,
                    message = errorCodes.getCodeMessage(code);
                return service.error(title, message);
            };

            service.pin = function(title, message, num) {
                var dialog = ngDialog.open({
                    name: 'pin',
                    controller: 'PinDialogController',
                    template: 'app/shared/templates/pinMessage.html',
                    disableAnimation: (tivoService.getBrowserConfig() === 'cisco' ? true : false),
                    data: {
                        title: title || $translate.instant('DEFAULT_INFO_TITLE'),
                        message: message || $translate.instant('DEFAULT_INFO_MESSAGE'),
                        char: '',
                        numChars: num
                    }
                });
                return dialog && dialog.closePromise;
            };

            service.pinCode = function(title, message, code, okFn) {
                var dialog = service.pin($translate.instant(title), $translate.instant(message), 4);
                if (dialog) {
                    dialog.then(function(data) {
                        var match = false, value = '', i=0;

                        if(code) {
                            if (data.value && data.value.toString() === code.toString()) {
                                match = true;
                            }
                        }else{
                            for(i=0; i<data.value.length; i+=1) {
                                value += keyCodes[data.value[i]];
                            }

                            if(tivoService.verifyParentalControlPIN(value)) {
                                match = true;
                            }
                        }

                        if (match) {
                            okFn();
                        } else {
                            $timeout(function() {
                                service.pinCode('PIN_TITLE_WRONG', code ? message : 'PIN_MESSAGE_WRONG', code, okFn);
                            }, 1000);
                        }
                    });
                } else if (okFn) {
                    $timeout(function() {
                        service.pinCode(title, message, code, okFn);
                    }, 2000);
                }
            };

            service.loading = function() {
                var dialog = null;
                if (!ngDialog.isOpen('loading-dialog')) {
                    dialog = ngDialog.open({
                        name: 'loading',
                        template: 'app/shared/templates/loader.html',
                        disableAnimation: (tivoService.getBrowserConfig() === 'cisco' ? true : false)
                    });
                }

                return dialog && dialog.closePromise;
            };

            service.loaded = function() {
                if (ngDialog.isOpen('loading-dialog')) {
                    ngDialog.close('loading-dialog');
                }
            };

            service.resetCodeMessage = function(key) {
                keyPressHistory = [];
            };

            service.codeMessagePressed = function(key) {
                keyPressHistory.push(key);
                if (keyPressHistory.length === appConfig.codeVersion.length) {
                    if (keyPressHistory.toString() === appConfig.codeVersion.toString()) {
                        service.info(null, $translate.instant('VERSION_MESSAGE') + appConfig.appVersion +
                        ' (' + (tivoService.getBrowserConfig().toUpperCase() + '-' + tivoService.getAVInputType() + ')'))
                    .then(function(value) {
                        service.resetCodeMessage();
                    });
                    } else {
                        service.resetCodeMessage();
                    }
                }
            };

            return service;
        }]).controller('RatingController', ['$scope', 'ngDialog', 'movement',
            function($scope, ngDialog, movement) {
                var dialogData = $scope.ngDialogData,
                    BAR_WIDTH= 415,
                    OUT_OF = 5,
                    BALL_WIDTH=54,
                    idx=0;

                $scope.title = (dialogData.title) ? dialogData.title : '';
                $scope.rating = (dialogData.rating) ? parseInt(dialogData.rating) : 0;
                idx = (dialogData.rating) ? parseInt(dialogData.rating) : 0;
                $scope.buttons = dialogData.buttons ? dialogData.buttons : [{
                    type: 'button',
                    title: 'SAVE_RATING',
                    classLi: 'green60 login-button'
                }];
                $scope.styleBar= {};

                $scope.listConf = {
                    circular: false,
                    direction: 1,
                    focus: false,
                    focusIndex: dialogData.focusIndex,
                    selectedIndex: dialogData.focusIndex,
                    onSelect: vote,
                    item: {
                        template: 'app/shared/templates/messageButtonItem.html'
                    }
                };
                $scope.listP = {};
                $scope.listStyle = {};
                $scope.ballBarStyle= {};
                $scope.styleBar= {};

                ratingBarMovement(idx);
            /**
             * Calculate the interval to move the ball bar
             * @return {number} the size of the offset
             */
                function interval() {
                    return (BAR_WIDTH-BALL_WIDTH) / OUT_OF;
                };
            /**
             * Generate the bar movement
             * @param {number} id
             */
                function ratingBarMovement(id) {
                    $scope.ballBarStyle= movement.getTransform((id*interval()), 0, true);
                    $scope.styleBar=movement.getWidth((id*interval())+12);
                }
            /**
             * Set the rate of an asset
             * @param {number} rating
             */
                function vote() {

                }


            /**
             * Key handler method that handles key presses.
             * @method keyHandler
             * @param {String} key The key that was pressed.
             * @return {Boolean} True if the key press was handled, false if the
             * key press wasn't handled.
             */
                $scope.keyHandler = function(key) {
                    var handled = $scope.listP.keyHandler(key);

                    if(!handled) {
                        handled = true;
                        switch (key) {
                        case 'SELECT':
                            vote();
                            $scope.closeThisDialog();
                            break;
                        case 'BACK':
                        case 'HISTORY_BACK':
                            $scope.closeThisDialog();
                            break;
                        case 'UP':
                            $scope.listP.unHighlight();
                            break;
                        case 'DOWN':
                            $scope.listP.highlight();
                            break;
                        case 'LEFT':
                            if (!$scope.listP.isFocused()) {
                                if (idx > 0) {
                                    idx--;
                                    $scope.rating-=1;
                                    ratingBarMovement(idx);
                                }
                            }
                            break;
                        case 'RIGHT':
                            if (!$scope.listP.isFocused()) {
                                if(idx < OUT_OF) {
                                    idx++;
                                    $scope.rating+=1;
                                    ratingBarMovement(idx);
                                }
                            }
                            break;
                        }
                    }
                    return handled;
                };
            }]);

