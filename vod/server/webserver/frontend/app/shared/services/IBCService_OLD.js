/* eslint new-cap: ["error", { "capIsNew": false }]*/

'use strict';

angular.module('app')

    .factory('IBCService', ['$http', '$q', '$timeout', 'appConfig', 'messageService', 'tivoService', 'filterFilter',
        function($http, $q, $timeout, appConfig, messageService, tivoService, filterFilter) {
            var service = {};

            service.menu = [];
            service.genre = [];
            service.category = null;
            service.items = {};
            service.stack = [];
            service.edges = {};

            service.getMenu = function() {
                if (service.menu.length) {
                    var deferred = $q.defer();
                    deferred.resolve(service.menu);
                    return deferred.promise;
                }

                return $http
                .get(appConfig.backend + appConfig.path + 'category')
                .then(function(resp) {
                    if (resp.data && resp.data.length > 0) {
                        service.menu = resp.data;
                    }
                    return resp.data;
                });
            };

            service.getAsset = function(data) {
                return $http
                .get(appConfig.backend + appConfig.path + 'asset/' + data)
                .then(function(resp) {
                    if (resp.data && resp.data.items.length > 0) {
                        service.genre = resp.data;
                    }
                    return resp.data;
                });
            };

            service.getRelated = function(tags) {
                var spTags = tags ? tags.split(' ') : [], params = {};

                if(spTags.length) {
                    params.tags = spTags[0];
                }

                return $http
                .get(appConfig.backend + appConfig.path + 'asset/', {params: params})
                .then(function(resp) {
                    return resp.data;
                });
            };

            service.getEdges = function() {
                return $http
                .get(appConfig.backend + appConfig.path + 'service/edges')
                .then(function(resp){
                    if (resp.data) {
                        service.edges = resp.data;
                    }
                    return resp.data;
                });
            };

            service.getUrlImg = function() {
                return appConfig.backend + appConfig.pathImg;
            };

            service.setSelectedCategory = function(cat) {
                service.stack = [];
                service.category = cat;
            };

            service.getSelectedCategory = function() {
                return service.category;
            };

            service.addToStack = function(cat) {
                service.stack.push(cat);
            };

            service.removeFromStack = function(cat) {
                return service.stack.pop();
            };

            service.getStack = function(cat) {
                return service.stack;
            };

            return service;
        }]);
