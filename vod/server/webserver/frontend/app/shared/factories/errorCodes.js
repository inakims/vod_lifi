/* eslint new-cap: ["error", { "capIsNew": false }]*/

'use strict';

angular.module('app')

    .factory('errorCodes', ['$translate', function($translate) {
        var service = {};

        service.codes = {
            SETUP_LOGIN: {
                NUM: '100',
                MESSAGE: 'COD_ERROR_SETUP'
            },
            SETUP_SUBSCRIBE: {
                NUM: '101',
                MESSAGE: 'COD_ERROR_SETUP'
            },
            SETUP_MENU: {
                NUM: '102',
                MESSAGE: 'COD_ERROR_SETUP'
            },
            SETUP_DEVICE: {
                NUM: '103',
                MESSAGE: 'COD_ERROR_SETUP'
            },
            MENU_EMPTY: {
                NUM: '200',
                MESSAGE: 'COD_ERROR_SETUP'
            },
            MOSAIC_CATEGORY: {
                NUM: '300',
                MESSAGE: 'COD_ERROR_MOSAIC'
            },
            MOSAIC_URL: {
                NUM: '301',
                MESSAGE: 'COD_ERROR_MOSAIC'
            },
            MOSAIC_EMPTY: {
                NUM: '302',
                MESSAGE: 'COD_ERROR_MOSAIC'
            },
            MOSAIC_ERROR: {
                NUM: '303',
                MESSAGE: 'COD_ERROR_MOSAIC'
            },
            PIP_ASSET: {
                NUM: '400',
                MESSAGE: 'COD_ERROR_PIP'
            },
            PIP_URL: {
                NUM: '401',
                MESSAGE: 'COD_ERROR_PIP'
            },
            SERVICE_CATEGORIES: {
                NUM: '500',
                MESSAGE: 'COD_ERROR_SERVICE'
            },
            SERVICE_CATEGORY: {
                NUM: '501',
                MESSAGE: 'COD_ERROR_SERVICE'
            },
            SERVICE_ASSET: {
                NUM: '502',
                MESSAGE: 'COD_ERROR_SERVICE'
            }
        };

        service.getCodeMessage = function(key) {
            var code = service.codes[key],
                message = '';
            if (code) {
                message = $translate.instant('COD_ERROR') + code.NUM + ': ' + $translate.instant(code.MESSAGE);
            }
            return message;
        };

        return service;
    }]);
