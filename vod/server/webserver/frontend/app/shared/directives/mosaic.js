'use strict';

(function() {
    angular.module('app')
        .directive('mosaic', [function() {
            return {
                restrict: 'E',
                scope: {
                    'config': '=',
                    'data': '=',
                    'interface': '='
                },
                template:
                    '<ul class="list-inline">' +
                        '<ng-include src="getTemplateUrl()" style="position:absolute;left:0px; top:1px"/>' +
                    '</ul>',
                controller: ['$scope', function($scope) {
                    var col = 0, row = 0, maxRows = 0, selectedRow = 0,
                        onSelect = $scope.config.onSelect,
                        animRows = $scope.config.animRows || 0,
                        focusedRow = $scope.config.focusedRow ? $scope.config.focusedRow + animRows : 0,
                        onChange = $scope.config.onChange || function() {};

                    $scope.focus = $scope.config.focus || false;
                    $scope.tiles = [];

                    $scope.getTemplateUrl = function() {
                        return $scope.config.item.template;
                    };

                    $scope.highlight = function() {
                        return $scope.focus = true;
                    };

                    $scope.unHighlight = function() {
                        return $scope.focus = false;
                    };

                    $scope.isFocused = function() {
                        return $scope.focus;
                    };

                    $scope.getSelectedIndex = function() {
                        return $scope.focusIndex;
                    };

                    $scope.getIndexPosition = function() {
                        return {
                            row: row,
                            col: col
                        };
                    };

                    $scope.getSelectedItem = function() {
                        return $scope.tiles[$scope.focusIndex];
                    };

                    /**
                    * add animation nodes
                    */
                    function draw() {
                        var start = 0, end = 0, count = 0;

                        start = (row - focusedRow - 1) * $scope.config.cols;
                        end = start + ($scope.config.rows + animRows) * $scope.config.cols;
                        end = end > $scope.data ? $scope.data -1 : end;

                        if(row === animRows || start<0) {
                            // hidden nodes
                            $scope.tiles = [];

                            // fill hidden nodes if no enought data
                            while($scope.tiles.length < animRows * $scope.config.cols) {
                                $scope.tiles.push($scope.config.getDefAsset($scope.data[0], count));
                                count +=1;
                            }

                            // visible nodes
                            $scope.tiles = $scope.tiles.concat($scope.data.slice(0, $scope.config.rows * $scope.config.cols));
                        }else{
                            $scope.tiles = $scope.data.slice(start, end);
                        }
                    }

                    /**
                    * update mosaic items
                    * @param {string} key
                    */
                    function scroll(key) {
                        var repaint = false;

                        if($scope.config.rows) {
                            switch(key) {
                            case 'DOWN':
                                repaint = row > focusedRow;
                                break;
                            case 'UP':
                                repaint = row >= focusedRow;
                                break;
                            }

                            if(repaint) {
                                selectedRow = focusedRow;
                                draw();
                            }
                        }

                        if(!repaint) {
                            selectedRow = row;
                        }

                        $scope.focusIndex = selectedRow * $scope.config.cols + col;
                        checkPageStatus();

                        onChange($scope.tiles[$scope.focusIndex], key, $scope.focusIndex, repaint, row);
                    }

                    /**
                    * Check the page status
                    * @param {boolean} prev
                    */
                    function checkPageStatus() {
                        var dataIndex = (row - animRows) * $scope.config.cols + col;
                        if ($scope.data.length - dataIndex <= $scope.config.pageSize / 2) {
                            $scope.config.onPage(dataIndex);
                        }
                    }

                    /**
                    * Select previous row.
                    * @return {boolean} handled
                    */
                    function selectPreviousRow() {
                        var ret = false;

                        if($scope.focus) {
                            if(row === animRows) {
                                if($scope.config.circular) {
                                    row = maxRows - 1;
                                    ret = true;
                                }
                            }else{
                                row -= 1;
                                ret = true;
                            }
                        }

                        if(ret) {
                            scroll('UP');
                        }

                        return ret;
                    }

                    /**
                    * Select next row.
                    * @return {boolean} handled
                    */
                    function selectNextRow() {
                        var ret = false;

                        if($scope.focus) {
                            if(row === (maxRows -1)) {
                                if($scope.config.circular) {
                                    row = 0;
                                    ret = true;
                                }
                            }else{
                                row += 1;

                                // select next available col
                                if(row === maxRows -1 && ((row - animRows) * $scope.config.cols + col) > $scope.data.length -1) {
                                    col = $scope.data.length - (row - animRows) * $scope.config.cols - 1;
                                }

                                ret = true;
                            }
                        }

                        if(ret) {
                            scroll('DOWN');
                        }

                        return ret;
                    }

                    /**
                    * Select previous item on the row.
                    * @return {boolean} handled
                    */
                    function selectPreviousCol() {
                        var ret = false;

                        if($scope.focus) {
                            if(col === 0) {
                                if($scope.config.circular) {
                                    col = $scope.config.cols -1;
                                    ret = true;
                                }
                            }else{
                                col -= 1;
                                ret = true;
                            }
                        }

                        if(ret) {
                            $scope.focusIndex = selectedRow * $scope.config.cols + col;
                        }

                        return ret;
                    }

                    /**
                    * Select next item on the row.
                    * @return {boolean} handled
                    */
                    function selectNextCol() {
                        var ret = false;

                        if($scope.focus) {
                            if((col + 1) % $scope.config.cols === 0) {
                                if($scope.config.circular) {
                                    col = 0;
                                    ret = true;
                                }
                            }else if(row !== maxRows -1 || ((row - animRows) * $scope.config.cols + col) < $scope.data.length -1) {
                                col += 1;
                                ret = true;
                            }
                        }

                        if(ret) {
                            $scope.focusIndex = selectedRow * $scope.config.cols + col;
                            checkPageStatus();
                        }

                        return ret;
                    }

                    /**
                    * Select an index position
                    * @param {Object} position {col: X, row: X}
                    * @param {Boolean} repaint
                    */
                    $scope.selectIndex = function(position, repaint) {
                        col = position.col;
                        row = position.row;
                        selectedRow = row < focusedRow ? row : focusedRow;
                        $scope.focusIndex = selectedRow * $scope.config.cols + col;
                        checkPageStatus();
                        if (repaint) {
                            draw();
                        }
                    };

                    $scope.keyHandler = function(key) {
                        var handled = false;
                        switch (key) {
                        case 'UP':
                            handled = selectPreviousRow();
                            break;
                        case 'DOWN':
                            handled = selectNextRow();
                            break;
                        case 'LEFT':
                            handled = selectPreviousCol();
                            break;
                        case 'RIGHT':
                            handled = selectNextCol();
                            break;
                        case 'SELECT':
                            if (onSelect) {
                                handled = onSelect($scope.tiles[$scope.focusIndex]);
                            }
                            break;
                        }
                        return handled;
                    };

                    $scope.$watch('data', function() {
                        maxRows = Math.ceil($scope.data.length / $scope.config.cols) + animRows;
                        if ($scope.data.length && $scope.focusIndex == null) {
                            $scope.selectIndex($scope.config.focusIndex);
                        } else if (!$scope.data.length) {
                            $scope.focusIndex = null;
                        }

                        if ($scope.config.rows) {
                            draw();
                        } else {
                            $scope.tiles = $scope.data;
                        }
                    });
                }],
                link: function(scope, element, attrs, controller) {
                    if (scope.interface) {
                        scope.interface.highlight = scope.highlight;
                        scope.interface.unHighlight = scope.unHighlight;
                        scope.interface.getSelectedIndex = scope.getSelectedIndex;
                        scope.interface.getIndexPosition = scope.getIndexPosition;
                        scope.interface.getSelectedItem = scope.getSelectedItem;
                        scope.interface.isFocused = scope.isFocused;
                        scope.interface.selectIndex = scope.selectIndex;
                        scope.interface.keyHandler = scope.keyHandler;
                    }
                }
            };
        }]);
}());
