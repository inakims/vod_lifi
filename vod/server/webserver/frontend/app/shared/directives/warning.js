'use strict';

(function() {
    angular.module('app')
        .directive('warning', [function() {
            return {
                restrict: 'E',
                scope: {
                    'message': '='
                },
                templateUrl: 'app/shared/templates/warning.html'
            };
        }]);
}());
