'use strict';

(function() {
    angular.module('app')
        .directive('tags', [function() {
            return {
                restrict: 'E',
                scope: {
                    'data': '=',
                    'config': '=',
                    'interface': '='
                },
                templateUrl: 'app/shared/templates/tags.html',
                controller: ['$scope', 'placerTvService', 'messageService', '$translate', 'filterFilter', 'movement',
                    function($scope, placerTvService, messageService, $translate, filterFilter, movement) {
                        var LIST_ITEM_HEIGHT = 47,
                            CLUE_WIDTH = 78,
                            MAX_ITEMS = 5,
                            REMOVE_ALL = {
                                icon: 'fa-list',
                                Title: $translate.instant('CAT_REMOVE_FILTER'),
                                action: 'removeAll'
                            };

                        $scope.listConf = {
                            circular: false,
                            direction: 1,
                            focus: false,
                            focusIndex: 1,
                            onChange: moveFocus,
                            onSelect: function(item) {
                                if (item.action && $scope[item.action]) {
                                    $scope[item.action]();
                                } else {
                                    item.selected = !item.selected;

                                    if (item.selected) {
                                        if (!placerTvService.addFilter(item)) {
                                            messageService.info($translate.instant('MAX_FILTER'), $translate.instant('MAX_FILTER_MESSAGE'));
                                            item.selected = false;
                                        }
                                    } else {
                                        placerTvService.removeFilter(item);
                                    }
                                }
                            },
                            item: {
                                template: 'app/shared/templates/tagItem.html'
                            }
                        };

                        $scope.listP = {};
                        $scope.clueStyle = {};
                        $scope.listStyle = {};

                        /**
                        * Move selected category
                        * @param {object} item
                        * @param {string} key
                        * @param {number} idx
                        */
                        function moveFocus(item, key, idx) {
                            if (idx < MAX_ITEMS) {
                                $scope.listStyle = movement.getTransform(0, 0, 0.5);
                            } else if(idx < $scope.data.length - MAX_ITEMS + 1) {
                                $scope.listStyle = movement.getTransform(0, -idx * LIST_ITEM_HEIGHT + (LIST_ITEM_HEIGHT * MAX_ITEMS), 0.5);
                            }
                        }

                        /**
                        * Select tags from a tagList
                        * @param {Array} tagList
                        */
                        function selectTags(tagList) {
                            angular.forEach(tagList, function(item) {
                                item = filterFilter($scope.tags, {TagId: item.TagId});
                                if (item && item.length) {
                                    $scope.tags[$scope.tags.indexOf(item[0])].selected = true;
                                }
                            });
                        }

                        $scope.removeAll = function() {
                            angular.forEach($scope.tags, function(item) {
                                item.selected = false;
                            });
                            placerTvService.removeAllFilters();
                        };

                        $scope.highlight = function() {
                            $scope.clueStyle = movement.getWidthOpacity(0, 0);
                            return $scope.listP.highlight();
                        };

                        $scope.unHighlight = function() {
                            $scope.clueStyle = movement.getWidthOpacity(CLUE_WIDTH, 0);
                            return $scope.listP.unHighlight();
                        };

                        $scope.isFocused = function() {
                            return $scope.listP.isFocused();
                        };

                        $scope.selectTagIndex = function(index) {
                            $scope.listP.selectIndex(index);
                        };

                        $scope.keyHandler = function(key) {
                            return $scope.listP.keyHandler(key);
                        };

                        /**
                        * Update tags list
                        * @param {object} tags
                        */
                        function updateTags(tags) {
                            $scope.tags = [];
                            if (tags && tags.length) {
                                $scope.tags.push(REMOVE_ALL);
                                $scope.tags = $scope.tags.concat(angular.copy(tags));
                                selectTags($scope.config.selected);
                            }
                        }

                        $scope.$watch(function() {
                            return $scope.data;
                        }, updateTags, true);
                    }],
                link: function(scope, element, attrs, controller) {
                    if (scope.interface) {
                        scope.interface.highlight = scope.highlight;
                        scope.interface.unHighlight = scope.unHighlight;
                        scope.interface.isFocused = scope.isFocused;
                        scope.interface.selectTagIndex = scope.selectTagIndex;
                        scope.interface.removeAll = scope.removeAll;
                        scope.interface.keyHandler = scope.keyHandler;
                    }
                }
            };
        }]);
}());
