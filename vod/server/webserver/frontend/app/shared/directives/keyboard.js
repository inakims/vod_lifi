'use strict';

(function() {
    angular.module('app')

        .directive('keyboard', ['keyCodes', 'globalKeyHandler', function(keyCodes, globalKeyHandler) {
            return {
                restrict: 'A',
                link: function(scope, elem, attrs) {
                    var elemKeyHandler = scope.$eval(attrs.keyboard);

                    /**
                     * Register a element keyHandler in the golbarKeyHandler
                     * @method registerElemKeyHandler
                     * @param {Function} keyHandler to register
                     */
                    function registerElemKeyHandler(keyHandler) {
                        globalKeyHandler.addKeyHandler(keyHandler);
                        scope.$on('$destroy', function() {
                            globalKeyHandler.removeKeyHandler(keyHandler);
                        });
                    };

                    /**
                     * Register the keydown listeners in the window element
                     * for the list of keyCodes
                     * @method registerGlobalKeyHandler
                     */
                    function registerGlobalKeyHandler() {
                        var keysToHandle = {},
                            lastKey = null,
                            lastKeyTimestamp = null,
                            REPEAT_TIME = 300;

                        // Registers key codes
                        angular.forEach(keyCodes, function(key, name) {
                            keysToHandle[key] = {
                                name: name
                            };
                        });

                        /**
                        * Handle the key down event
                        * @param {Object} keyDown
                        * @return {Boolean} isRepeat
                        */
                        function checkRepeatKey(keyDown) {
                            var isRepeat = false,
                                keyDownTimestamp = new Date();

                            if (lastKey && lastKey.name === keyDown.name) {
                                isRepeat = keyDownTimestamp.getTime() - lastKeyTimestamp.getTime() < REPEAT_TIME;
                            }
                            lastKey = keyDown;
                            lastKeyTimestamp = keyDownTimestamp;
                            return isRepeat;
                        };

                        /**
                        * Handle the key down event
                        * @param {Object} event
                        */
                        function bindKeyDown(event) {
                            var keyDown = keysToHandle[event.key],
                                isRepeat = false;

                            // Handler is registered
                            if (keyDown) {
                                event.preventDefault();
                                isRepeat = checkRepeatKey(keyDown);

                                // Invoke the handler and digest
                                scope.$apply(function() {
                                    var ret = globalKeyHandler.excuteKeyHandlers(keyDown.name, isRepeat);

                                    if (ret && event.stopImmediatePropagation) {
                                        event.stopImmediatePropagation();
                                    }
                                });
                            }
                        };

                        angular.element(window).on('keydown', bindKeyDown);
                        scope.$on('$destroy', function() {
                            angular.element(window).off('keydown', bindKeyDown);
                        });
                    };

                    if (elemKeyHandler) {
                        registerElemKeyHandler(elemKeyHandler);
                    } else {
                        registerGlobalKeyHandler();
                    }
                }
            };
        }]);
}());
