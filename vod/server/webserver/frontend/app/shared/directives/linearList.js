'use strict';

(function() {
    angular.module('app')
        .directive('linearList', [function() {
            return {
                restrict: 'E',
                scope: {
                    'config': '=',
                    'data': '=',
                    'interface': '='
                },
                template: '<ul class="list-inline"><ng-include src="getTemplateUrl()"/></ul>',
                controller: ['$scope', function($scope) {
                    var Direction = {
                            HORIZONTAL: 0,
                            VERTICAL: 1,
                            Horizontal: 0,
                            Vertical: 1
                        },
                        direction = $scope.config.direction || Direction.HORIZONTAL,
                        onChange = $scope.config.onChange || function() {},
                        onSelect = $scope.config.onSelect;

                    $scope.focus = $scope.config.focus || false;
                    $scope.focusIndex = $scope.config.focusIndex || 0;

                    $scope.getTemplateUrl = function() {
                        return $scope.config.item.template;
                    };

                    $scope.highlight = function() {
                        return $scope.focus = true;
                    };

                    $scope.unHighlight = function() {
                        return $scope.focus = false;
                    };

                    $scope.isFocused = function() {
                        return $scope.focus;
                    };

                    $scope.getSelectedIndex = function() {
                        return $scope.focusIndex;
                    };

                    $scope.getSelectedItem = function() {
                        return $scope.data[$scope.focusIndex];
                    };

                    $scope.selectIndex = function(index) {
                        if ($scope.data && index >= 0 && index < $scope.data.length) {
                            $scope.focusIndex = index;
                            onChange($scope.data[$scope.focusIndex], null, $scope.focusIndex);
                        }
                    };

                    /**
                    * Select previous item on the list.
                    * @param {string} key
                    * @return {boolean} handled
                    */
                    function selectPrevious(key) {
                        var ret = false,
                            prevIndex = null,
                            i = $scope.focusIndex - 1;

                        if ($scope.focus) {
                            if ($scope.config.circular) {
                                while (i !== $scope.focusIndex && prevIndex === null) {
                                    if (i === 0) {
                                        i = $scope.data.length - 1;
                                    }
                                    if (!$scope.data[i].disable) {
                                        prevIndex = i;
                                    }
                                    i--;
                                }
                            } else {
                                while (i >= 0 && prevIndex === null) {
                                    if (!$scope.data[i].disable) {
                                        prevIndex = i;
                                    }
                                    i--;
                                }
                            }

                            if (prevIndex != null) {
                                ret = true;
                                $scope.focusIndex = prevIndex;
                                onChange($scope.data[$scope.focusIndex], key, $scope.focusIndex);
                            }
                        }

                        return ret;
                    }

                    /**
                    * Select next item on the list.
                    * @param {string} key
                    * @return {boolean} handled
                    */
                    function selectNext(key) {
                        var ret = false,
                            nextIndex = null,
                            i = $scope.focusIndex + 1;

                        if ($scope.focus) {
                            if ($scope.config.circular) {
                                while (i !== $scope.focusIndex && nextIndex === null) {
                                    if (i === $scope.data.length) {
                                        i = 0;
                                    }
                                    if (!$scope.data[i].disable) {
                                        nextIndex = i;
                                    }
                                    i++;
                                }
                            } else {
                                while (i < $scope.data.length && nextIndex === null) {
                                    if (!$scope.data[i].disable) {
                                        nextIndex = i;
                                    }
                                    i++;
                                }
                            }

                            if (nextIndex != null) {
                                ret = true;
                                $scope.focusIndex = nextIndex;
                                onChange($scope.data[$scope.focusIndex], key, $scope.focusIndex);
                            }
                        }

                        return ret;
                    }

                    $scope.keyHandler = function(key) {
                        var handled = false;
                        switch (key) {
                        case 'UP':
                            if (direction === Direction.VERTICAL) {
                                handled = selectPrevious(key);
                            }
                            break;
                        case 'DOWN':
                            if (direction === Direction.VERTICAL) {
                                handled = selectNext('DOWN');
                            }
                            break;
                        case 'LEFT':
                            if (direction === Direction.HORIZONTAL) {
                                handled = selectPrevious('LEFT');
                            }
                            break;
                        case 'RIGHT':
                            if (direction === Direction.HORIZONTAL) {
                                handled = selectNext('RIGHT');
                            }
                            break;
                        case 'SELECT':
                            if (onSelect) {
                                handled = onSelect($scope.data[$scope.focusIndex]);
                            }
                            break;
                        }
                        return handled;
                    };
                }],
                link: function(scope, element, attrs, controller) {
                    if (scope.interface) {
                        scope.interface.highlight = scope.highlight;
                        scope.interface.unHighlight = scope.unHighlight;
                        scope.interface.getSelectedIndex = scope.getSelectedIndex;
                        scope.interface.getSelectedItem = scope.getSelectedItem;
                        scope.interface.isFocused = scope.isFocused;
                        scope.interface.selectIndex = scope.selectIndex;
                        scope.interface.keyHandler = scope.keyHandler;
                    }
                }
            };
        }]);
}());
