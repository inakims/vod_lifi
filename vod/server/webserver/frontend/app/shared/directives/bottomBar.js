'use strict';

(function() {
    angular.module('app')
        .directive('bottomBar', ['$translate', 'tivoService', 'IBCService', function($translate, tivoService, IBCService) {
            return {
                restrict: 'E',
                scope: {
                    'data': '=',
                    'filter': '='
                },
                templateUrl: 'app/shared/templates/bottomBar.html',
                controller: ['$scope', function($scope) {
                    var browser = tivoService.getBrowserConfig(),
                        ICONS = {
                            BACK: {
                                textIcon: browser === 'velazquez' ? $translate.instant('') : '',
                                icon: 'fa-arrow-left',
                                Title: 'Return'
                            },
                            OK: {
                                textIcon: $translate.instant('OK'),
                                icon: '',
                                Title: $translate.instant('SELECT')
                            },
                            PLAY: {
                                textIcon: '',
                                icon: 'fa-play',
                                Title: $translate.instant('PLAY')
                            },
                            CAT_PREV: {
                                textIcon: '',
                                icon: 'fa-backward',
                                Title: $translate.instant('CAT_PREV')
                            },
                            CAT_NEXT: {
                                textIcon: '',
                                icon: 'fa-forward',
                                Title: $translate.instant('CAT_NEXT')
                            },
                            EXIT: {
                                textIcon: '',
                                icon: 'fa-circle-o ',
                                Title: $translate.instant('EXIT')
                            }
                        },
                        FILTER_ICONS = {
                            ADD: {
                                icon: 'green-icon fa-circle',
                                Title: $translate.instant('CAT_ADD_FILTER')
                            },
                            CURRENT: {
                                icon: 'fa-filter',
                                Title: $translate.instant('CURRENT_FILTER')
                            },
                            REMOVE: {
                                icon: 'red-icon fa-circle',
                                Title: $translate.instant('CAT_REMOVE_FILTER')
                            },
                            SCROLL_UP: {
                                icon: 'blue-icon fa-circle',
                                Title: $translate.instant('SCROLL_UP')
                            },
                            ELLIPSIS: {
                                icon: '',
                                Title: '...'
                            }
                        },
                        iconsList = [],
                        i = 0,
                        MAX_FILTERS_DISPLAY = 3;
                        // botones=['BACK', 'OK'];
                    // if ($scope.data) {
                    //     for (i = 0; i < $scope.data.length; i++) {
                    //         iconsList.push(ICONS[$scope.data[i]]);
                    //     }
                    // }


                    // if(IBCService.category.type=='category') {
                    //     for (i = 0; i <botones.length; i++) {
                    //         iconsList.push(ICONS[botones[i]]);
                    //     }
                    // }
                    /**
                     * Update filters list
                     * @param {object} data
                     */
                    function updatebottonbar(data) {
                        iconsList=[];
                        for (i = 0; i < $scope.data.length; i++) {
                            iconsList.push(ICONS[$scope.data[i]]);
                        }
                        $scope.icons = iconsList;
                    }


                    if ($scope.filter !== null) {
                        /**
                        * Update filters list
                        * @param {object} filters
                        */
                        function updateFilters(filters) {
                            $scope.filters = [];
                            if (filters) {
                                if (filters.cat) {
                                    if (filters.cat.length) {
                                        $scope.filters = $scope.filters.concat(filters.cat.slice(0, MAX_FILTERS_DISPLAY));
                                        if (filters.cat.length > MAX_FILTERS_DISPLAY) {
                                            $scope.filters.unshift(FILTER_ICONS.ELLIPSIS);
                                        }
                                        $scope.filters.push(FILTER_ICONS.CURRENT);
                                        $scope.filters.unshift(FILTER_ICONS.REMOVE);
                                    }

                                    $scope.filters.push(FILTER_ICONS.ADD);
                                }

                                if(filters.init) {
                                    $scope.filters.push({
                                        icon: 'yellow-icon fa-circle',
                                        Title: filters.init
                                    });
                                }

                                if(filters.scroll) {
                                    $scope.filters.push(FILTER_ICONS.SCROLL_UP);
                                }
                            }
                        }

                        $scope.$watch(function() {
                            return $scope.filter;
                        }, updateFilters, true);

                        $scope.$watch(function() {
                            return $scope.data;
                        }, updatebottonbar, true);
                    }
                }]
            };
        }]);
}());
