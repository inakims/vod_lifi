'use strict';

(function() {
    angular.module('app')
        .directive('player', [function() {
            return {
                restrict: 'E',
                scope: {
                    'config': '=',
                    'interface': '='
                },
                templateUrl: 'app/shared/templates/video.html',
                controller: ['$scope', '$timeout', '$interval', 'VG_STATES', 'tivoService', '$translate', '$state', '$stateParams',
                    function($scope, $timeout, $interval, VG_STATES, tivoService, $translate, $state, $stateParams) {
                        var browser = tivoService.getBrowserConfig(),
                            PLAYBACK_STATUS = {
                                RW: 'Rewind',
                                FF: 'Fastforward',
                                STOP: 'Stop',
                                PLAY: 'Play',
                                PAUSE: 'Pause'
                            },
                            SEEK_UPDATE = 1000,
                            isFocused = true,
                            bannerTout = null,
                            seekTimeInterval = null;

                        /**
                         * Reset the speed to 1
                         */
                        function resetSpeed() {
                            clearSeekTimeInterval();
                            $scope.playbackSpeedIndex = 0;
                        }

                        /**
                         * Clear the seek time interval.
                         */
                        function clearSeekTimeInterval() {
                            if (seekTimeInterval) {
                                $interval.cancel(seekTimeInterval);
                                seekTimeInterval = null;
                            }
                        }

                        /**
                         * Set the seek time interval.
                         * @param {Integer} speed
                         */
                        function setSeekTimeInterval(speed) {
                            var seekRate = speed * SEEK_UPDATE,
                                seekTime = null;

                            /**
                             * Control to play media.
                             */
                            function seekFunction() {
                                if ($scope.playbackStatus === PLAYBACK_STATUS.RW) {
                                    seekTime = $scope.API.currentTime - seekRate;
                                    if (seekTime < 0) {
                                        $scope.API.seekTime(0);
                                        onClickPlay();
                                    } else {
                                        $scope.API.seekTime(seekTime / 1000);
                                    }
                                } else if ($scope.playbackStatus === PLAYBACK_STATUS.FF) {
                                    seekTime = $scope.API.currentTime + seekRate;
                                    if (seekTime > $scope.API.totalTime) {
                                        onClickPlay();
                                        $scope.API.seekTime($scope.API.totalTime);
                                    } else {
                                        $scope.API.seekTime(seekTime / 1000);
                                    }
                                } else {
                                    resetSpeed();
                                }
                            }

                            clearSeekTimeInterval();
                            seekFunction();
                            seekTimeInterval = $interval(seekFunction, SEEK_UPDATE);
                        };

                        /**
                         * Play / pause / stop media.
                         * @param {PLAYBACK_STATUS} status
                         * @param {Boolean} fullscreen
                         */
                        function trickChange(status, fullscreen) {
                            if ($scope.API && $scope.playbackStatus !== status) {
                                if ($scope.isSeekingTime()) {
                                    resetSpeed();
                                    setPlayPauseIcon(status);
                                }

                                if (fullscreen && $scope.playbackStatus === PLAYBACK_STATUS.STOP && !$scope.API.isFullScreen) {
                                    $scope.API.toggleFullScreen();
                                }

                                if (status === PLAYBACK_STATUS.PLAY) {
                                    $scope.API.play();
                                } else if (status === PLAYBACK_STATUS.PAUSE) {
                                    $scope.API.pause();
                                }
                            }
                        }

                        /**
                         * Control to play media.
                         */
                        function onClickPlay() {
                            trickChange(PLAYBACK_STATUS.PLAY, true);
                        }

                        /**
                         * Control to pause media.
                         */
                        function onClickPause() {
                            trickChange(PLAYBACK_STATUS.PAUSE, false);
                        }

                        /**
                         * Control to play and pause media.
                         */
                        function onClickPlayPause() {
                            if ($scope.isSeekingTime() || $scope.playbackStatus === PLAYBACK_STATUS.STOP) {
                                trickChange(PLAYBACK_STATUS.PLAY, true);
                            } else {
                                $scope.API.playPause();
                            }
                        }

                        /**
                         * Control to toggle fullscreen.
                         */
                        /* function onClickFullScreen() {
                            $scope.API.toggleFullScreen();
                        }*/

                        /**
                         * Control to change playback speed.
                         * @param {Boolean} rewind
                         */
                        function onClickPlayback(rewind) {
                            var newSpeedIndex = null,
                                status = rewind ? PLAYBACK_STATUS.RW : PLAYBACK_STATUS.FF,
                                reverseStatus = rewind ? PLAYBACK_STATUS.FF : PLAYBACK_STATUS.RW,
                                buttonIndex = rewind ? 1 : 3;

                            if ($scope.playbackStatus !== PLAYBACK_STATUS.STOP) {
                                if ($scope.playbackStatus === reverseStatus) {
                                    newSpeedIndex = $scope.playbackSpeedIndex = 1;
                                } else if ($scope.playbackSpeedIndex < $scope.config.speeds.length - 1) {
                                    newSpeedIndex = ++$scope.playbackSpeedIndex;
                                }

                                if (newSpeedIndex) {
                                    $scope.selectButtonIndex(buttonIndex);
                                    setPlayPauseIcon(PLAYBACK_STATUS.PAUSE);
                                    $scope.playbackStatus = status;
                                    setSeekTimeInterval($scope.config.speeds[newSpeedIndex]);
                                }
                            }
                        };

                        /**
                         * Set the timeout to hide the banner.
                         */
                        function setBannerTimeout() {
                            if ($scope.config.autoHideTime) {
                                bannerTout && $timeout.cancel(bannerTout);
                                bannerTout = $timeout(function() {
                                    hideBanner();
                                    bannerTout && $timeout.cancel(bannerTout);
                                    bannerTout = null;
                                }, $scope.config.autoHideTime);
                            }
                        }

                        /**
                         * Display the banner.
                         */
                        function showBanner() {
                            if (!$scope.showBanner) {
                                $scope.showBanner = true;
                                $scope.listP.highlight();
                            }
                            setBannerTimeout();
                        }

                        /**
                         * Hide the banner when is fullscreen mode.
                         */
                        function hideBanner() {
                            $scope.showBanner = !$scope.API.isFullScreen || $scope.isSeekingTime();
                            if (!$scope.showBanner) {
                                $scope.listP.unHighlight();
                            }
                        }

                        /**
                         * Show body
                         * @param {Boolean} playing
                         */
                        function hideOverlay(playing) {
                            angular.element(document.body).css('display', 'block');
                        }

                        $scope.highlight = function() {
                            isFocused = true;
                            return $scope.listP.highlight();
                        };

                        $scope.unHighlight = function() {
                            isFocused = false;
                            return $scope.listP.unHighlight();
                        };

                        $scope.toggleFullScreen = function() {
                           // return $scope.API.toggleFullScreen();
                        };

                        $scope.isFocused = function() {
                            return isFocused;
                        };

                        $scope.isFullScreen = function() {
                            return $scope.API.isFullScreen;
                        };

                        $scope.isSeekingTime = function() {
                            return ($scope.playbackStatus === PLAYBACK_STATUS.RW ||
                                $scope.playbackStatus === PLAYBACK_STATUS.FF);
                        };

                        $scope.selectButtonIndex = function(index) {
                            $scope.listP.selectIndex(index);
                        };

                        $scope.onPlayerReady = function(API) {
                            $scope.API = API;
                        };

                        $scope.onCanPlay = function(event) {
                            $scope.loading = false;
                            $scope.playerBlocked = false;
                            if ($scope.config.autoStart) {
                                hideOverlay();
                                $scope.config.autoStart = false;
                                if (!$scope.API.isFullScreen) {
                                    $scope.API.toggleFullScreen();
                                }

                                onClickPlay();
                            }
                        };

                        $scope.onError = function(event) {
                            /* $scope.loading = false;
                            $scope.playerBlocked = true;
                            if ($scope.config.autoStart) {
                                hideOverlay();
                                $scope.config.autoStart = false;
                            }

                            if ($scope.playbackStatus !== PLAYBACK_STATUS.STOP) {
                                if ($scope.API.isFullScreen) {
                                    $scope.API.toggleFullScreen();
                                    showBanner();
                                }
                                resetSpeed();
                                $scope.playbackStatus = PLAYBACK_STATUS.STOP;
                            } else {
                                $scope.API && $scope.API.stop();
                            }
                            $scope.config.onError && $scope.config.onError();*/
                        };

                        $scope.finishPlayback = function() {
                            resetSpeed();
                            if ($scope.playbackStatus !== PLAYBACK_STATUS.STOP) {
                                $scope.playbackStatus = PLAYBACK_STATUS.STOP;
                                $scope.API && $scope.API.stop();
                            }
                        };

                        /*$scope.initFullScreen = function() {
                            var videogular = angular.element(document.querySelector('#vgtest'));
                            videogular.addClass('fullscreen');
                            videogular.css('z-index', 20);
                            hideOverlay();
                        };*/

                        $scope.setLoading = function() {
                            $scope.loading = true;
                            if ($scope.playerBlocked) {
                                $scope.playerBlocked = false;
                                $scope.API.currentState = VG_STATES.STOP;
                            } else {
                                setPlayPauseIcon(PLAYBACK_STATUS.STOP);
                            }
                        };

                        $scope.getPlayerIcon = function() {
                            var ret = {};
                            switch ($scope.playbackStatus) {
                            case PLAYBACK_STATUS.PLAY:
                                break;
                            case PLAYBACK_STATUS.STOP:
                            case PLAYBACK_STATUS.PAUSE:
                                ret['fa fa-play'] = true;
                                break;
                            case PLAYBACK_STATUS.RW:
                                ret['fa fa-backward'] = true;
                                break;
                            case PLAYBACK_STATUS.FF:
                                ret['fa fa-forward'] = true;
                                break;
                            }
                            return ret;
                        };

                        $scope.buttons = [
                            {
                                title: 'BANNER_RETURN',
                                icon: browser === 'velazquez' ? '' : 'fa-undo',
                                topIcon: '',
                                textIcon: '',
                                action: $scope.config.onExit,
                                disable: true
                            },
                            {
                                title: '',
                                icon: 'fa-backward',
                                textIcon: '',
                                action: function() {
                                    onClickPlayback(true);
                                },
                                disable: true
                            },
                            {
                                title: '',
                                icon: 'fa-play',
                                textIcon: '',
                                action: onClickPlayPause,
                                disable: false
                            },
                            {
                                title: '',
                                icon: 'fa-forward',
                                textIcon: '',
                                action: function() {
                                    onClickPlayback(false);
                                },
                                disable: true
                            }
                        ];

                        $scope.listConf = {
                            circular: false,
                            direction: 0,
                            focus: true,
                            focusIndex: 1,
                            onChange: setBannerTimeout,
                            onSelect: function(item) {
                                if (!$scope.playerBlocked && !$scope.loading && $scope.API) {
                                    item.action();
                                    setBannerTimeout();
                                } else if ($scope.playerBlocked) {
                                    $scope.config.onError && $scope.config.onError();
                                }
                                return true;
                            },
                            item: {
                                template: 'app/shared/templates/bannerButtonItem.html'
                            }
                        };
                        $scope.listP = {};
                        $scope.listStyle = {};

                        /**
                         * keyhandler for trickplay
                         * @param {String} key
                         * @return {Boolean} handled
                         */
                        function trickKeyHandler(key) {
                            var handled = false;
                            if ($scope.playerBlocked || $scope.loading) {
                                switch (key) {
                                case 'PLAY':
                                case 'PAUSE':
                                case 'PLAY_PAUSE':
                                case 'STOP':
                                case 'REWIND':
                                case 'FAST_FORWARD':
                                case 'SLOW':
                                    if (browser === 'velazquez') {
                                        break;
                                    }
                                case 'SKIP_AHEAD':
                                    if ($scope.playerBlocked) {
                                        $scope.config.onError && $scope.config.onError();
                                    }
                                    handled = true;
                                    break;
                                }
                            } else {
                                switch (key) {
                                case 'PLAY':
                                    onClickPlay();
                                    $scope.selectButtonIndex(1);
                                    handled = true;
                                    break;
                                case 'PAUSE':
                                    onClickPause();
                                    $scope.selectButtonIndex(1);
                                    handled = true;
                                    break;
                                case 'PLAY_PAUSE':
                                    onClickPlayPause();
                                    $scope.selectButtonIndex(1);
                                    handled = true;
                                    break;
                                case 'STOP':
                                    $scope.finishPlayback();
                                    $scope.selectButtonIndex(1);
                                    handled = true;
                                    break;
                                case 'REWIND':
                                    onClickPlayback(true);
                                    handled = true;
                                    break;
                                case 'FAST_FORWARD':
                                    onClickPlayback(false);
                                    handled = true;
                                    break;
                                case 'SLOW':
                                    if (browser === 'velazquez') {
                                        break;
                                    }
                                case 'SKIP_AHEAD':
                                    if (!$scope.API.isFullScreen) {
                                        $scope.API.toggleFullScreen();
                                    }
                                    handled = true;
                                    break;
                                }

                                if (handled && !$scope.showBanner) {
                                    showBanner();
                                }
                            }

                            return handled;
                        }


                        $scope.keyHandler = function(key) {
                            var handled = false;
                            if (!$scope.showBanner || key === 'BACK') {
                                switch (key) {
                                case 'BACK':
                                    $scope.config.onExit();
                                    break;
                                case 'UP':
                                case 'DOWN':
                                case 'LEFT':
                                case 'RIGHT':
                                case 'SELECT':
                                    showBanner();
                                    handled = true;
                                    break;
                                }
                            } else if ($scope.listP.isFocused()) {
                                // Banner button list
                                handled = $scope.listP.keyHandler(key);
                            }

                            if (!handled && $scope.API) {
                                handled = trickKeyHandler(key);
                            }

                            return handled;
                        };

                        /**
                         * Set the playback speed icons.
                         * @param {VG_STATES} state
                         */
                        function setPlayPauseIcon(state) {
                            switch (state) {
                            case VG_STATES.PLAY:
                            case PLAYBACK_STATUS.PLAY:
                                $scope.buttons[2].icon = 'fa-pause';
                                $scope.buttons[2].title = '';
                                $scope.buttons[0].disable = false;
                                $scope.buttons[1].disable = false;
                                $scope.buttons[3].disable = false;
                                $scope.playbackStatus = PLAYBACK_STATUS.PLAY;
                                break;

                            case VG_STATES.PAUSE:
                            case PLAYBACK_STATUS.PAUSE:
                                $scope.buttons[2].icon = 'fa-play';
                                $scope.buttons[2].title = '';
                                $scope.buttons[0].disable = false;
                                $scope.buttons[1].disable = false;
                                $scope.buttons[3].disable = false;
                                $scope.playbackStatus = PLAYBACK_STATUS.PAUSE;
                                break;

                            case VG_STATES.STOP:
                            case PLAYBACK_STATUS.STOP:
                                $scope.buttons[2].icon = $scope.loading ? '' : 'fa-play';
                                $scope.buttons[2].title = $scope.loading ? 'LOADING' : 'BANNER_STOP';
                                $scope.buttons[0].disable = true;
                                $scope.buttons[1].disable = true;
                                $scope.buttons[3].disable = true;
                                if ($scope.playbackStatus !== PLAYBACK_STATUS.STOP) {
                                    $scope.playbackStatus = PLAYBACK_STATUS.STOP;
                                    $scope.selectButtonIndex(1);
                                    resetSpeed();
                                }

                                if ($scope.API && $scope.API.isFullScreen) {
                                    $scope.config.onExit();
                                }
                                break;
                            }
                        }

                        $scope.$watch(function() {
                            return $scope.API && $scope.API.currentState;
                        }, setPlayPauseIcon);

                        /**
                         * Set the playback speed icons.
                         * @param {Boolean} newVal
                         * @param {Boolean} oldVal
                         */
                        function setFullscreenIcon(newVal, oldVal) {
                            if (newVal || $scope.config.autoStart) {
                                $scope.buttons[0].title = 'BANNER_RETURN';
                                $scope.buttons[0].icon = browser === 'velazquez' ? 'fa-undo' : 'fa-undo'; ;
                                $scope.buttons[0].topIcon = '';
                                $scope.buttons[0].textIcon = '';
                            } else {
                                $scope.buttons[0].title = '';
                                $scope.buttons[0].icon = browser === 'velazquez' ? 'fa-expand' : 'fa-expand';
                                $scope.buttons[0].topIcon = '';
                                $scope.buttons[0].textIcon = '';
                            }
                            showBanner();
                        }

                        $scope.$watch(function() {
                            return $scope.API && $scope.API.isFullScreen;
                        }, setFullscreenIcon);

                        $scope.$watch(function() {
                            return tivoService.localSys.networkStatus;
                        }, function(newVal, oldVal) {
                            if ($scope.playerBlocked && newVal) {
                                $scope.playerBlocked = false;
                                onClickPlay();
                            }
                        });

                        $scope.$on('$destroy', $scope.finishPlayback);

                        $scope.playbackStatus = PLAYBACK_STATUS.STOP;
                        $scope.showBanner = true;
                        resetSpeed();
                    }],
                link: function(scope, element, attrs, controller) {
                    scope.setLoading();
                    if (scope.config.autoStart) {
                        scope.initFullScreen();
                    }

                    if (scope.interface) {
                        scope.interface.highlight = scope.highlight;
                        scope.interface.unHighlight = scope.unHighlight;
                        scope.interface.isFocused = scope.isFocused;
                        scope.interface.isFullScreen = scope.isFullScreen;
                        scope.interface.selectButtonIndex = scope.selectButtonIndex;
                        scope.interface.finishPlayback = scope.finishPlayback;
                        scope.interface.keyHandler = scope.keyHandler;
                        scope.interface.initFullScreen = scope.initFullScreen;
                        scope.interface.setLoading = scope.setLoading;
                    }
                }
            };
        }]);
}());
