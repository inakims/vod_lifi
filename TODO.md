#TODO AQX

-   [x] Crear modelo DB para servidores
-   [ ] ...

#### Crear DB con las IP de las CDN
 IPs:  
*   CDN1 --> 192.168.0.128
*   CDN2 --> 192.168.0.130
*   CDN3 --> 192.168.0.131

Como son IPs de uso privado, no se va a implementar (todavía) el análisis geográfico de las ip para ordenarlas. Se pasaran al cliente en el orden en el que estén en base de datos.

-   Crear ServersSchema y Servers Model

#### Integrar peticiones a la DB según se reciban req del cliente
Cuando el usuario pulsa play, se recibe req de transicion a PIP. Ahí se hará la petición a DB y se le manda al usuario la lista de las IPs.

El usuario carga esa lista de IPs e inicia la conexión con el primero de la lista

#### Establecer conexiones Q4S entre los servidores y el cliente
##### Servers
En cada uno de los servidores estará corriendo Server + Actuador Q4S a la espera de que se inicie el cliente4
##### Client
Cuando se reciba la respuesta del servidor con las IPs de las CDN, se iniciarán manualmente los clientes conectandose a esas IPs
